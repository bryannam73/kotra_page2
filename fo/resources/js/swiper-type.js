function setSwiperTab(idName, type) {
	const thisSwiper = document.getElementById(idName);
	const swiperSlide = thisSwiper.querySelectorAll('.swiper-slide:not(.swiper-slide-duplicate)');
	const swiperDuplicate = thisSwiper.querySelectorAll('.swiper-slide.swiper-slide-duplicate');
	let scrollA;

	if (type == 'link') {
		scrollA = thisSwiper.querySelectorAll('a');

		scrollA.forEach(event => {
			event.setAttribute('tabindex', '0');
		});
	} else if (type == 'overlay') {
		scrollA = thisSwiper.querySelectorAll('a');

		scrollA.forEach(event => {
			event.setAttribute('tabindex', '-1');
		});
	} else {
		swiperSlide.forEach(event => {
			event.setAttribute('tabindex', '-1');
			if (type == 'zoom') {
				event.childNodes[1].setAttribute('tabindex', '-1');
				event.childNodes[1].childNodes[1].setAttribute('tabindex', '0');
			} else {
				event.childNodes[1].setAttribute('tabindex', '0');
			}
		});
	}

	swiperDuplicate.forEach(event => {
		event.setAttribute('tabindex', '-1');
		event.childNodes[1].setAttribute('tabindex', '-1');
	});
}

function setSwiperAutoPlay(target, idName) {
	const thisSwiper = document.getElementById(idName);
	const thisBtn = thisSwiper.querySelector('.swiper-control-btn');

	thisBtn.addEventListener('click', () => {
		let autoPlayState = thisBtn.getAttribute('aria-pressed');

		if (autoPlayState === 'false') {
			thisBtn.setAttribute('aria-pressed', 'true');
			target.autoplay.stop();
			console.log(target.autoplay);
		} else if (autoPlayState === 'true') {
			thisBtn.setAttribute('aria-pressed', 'false');
			target.autoplay.start();
			console.log(target.autoplay);
		}
	});
}

function fullSwiper(idName, type) {
	let autoplay = false;
	if (type === 'auto') {
		autoplay = {
			delay: 3000,
			disableOnInteraction: false,
		};
	}

	let swiper = new Swiper('#' + idName, {
		slidesPerView: 1,
		// effect: 'fade',
		autoplay: autoplay,
		parallax: true,
		navigation: {
			nextEl: '#' + idName + ' .swiper-next',
			prevEl: '#' + idName + ' .swiper-prev',
		},
		pagination: {
			el: '#' + idName + ' .swiper-pagination', // 페이징 태그 클래스
			clickable: true,
		},
		loop: false,
		loopFillGroupWithBlank: true,
		ally: {
			enabled: true,
			prevSlideMessage: 'Prev Slide',
			nextSlideMessage: 'Next Slide',
			slideLabelMessage: 'Total {{slidesLength}} Slide {{index}} in the chapter',
		},
		container: {
			a11y: true,
			paginationElement: 'button',
		},
		on: {
			init: function () {
				if (type == 'auto') {
					setSwiperAutoPlay(this, idName);
				}
				setSwiperTab(idName, 'link');
			}, // init
		}, // e : on
	});

	return swiper;
}

function multiSwiper(type, idName, mobile, mobileGap, tablet, tabletGap, pc, pcGap) {
	let autoplay = false;
	if (type === 'auto') {
		autoplay = {
			delay: 3000,
			disableOnInteraction: false,
		};
	}

	let swiper = new Swiper('#' + idName, {
		slidesPerView: pc,
		spaceBetween: pcGap,
		slidesPerGroup: pc,
		threshold: 50,
		breakpoints: {
			280: {
				slidesPerView: mobile,
				spaceBetween: mobileGap,
				slidesPerGroup: mobile,
			},
			769: {
				slidesPerView: tablet,
				spaceBetween: tabletGap,
				slidesPerGroup: tablet,
			},
			1025: {
				slidesPerView: pc,
				spaceBetween: pcGap,
				slidesPerGroup: pc,
			},
		},
		autoplay: autoplay,
		navigation: {
			nextEl: '#' + idName + ' .swiper-next',
			prevEl: '#' + idName + ' .swiper-prev',
		},
		pagination: {
			el: '#' + idName + ' .swiper-pagination', // 페이징 태그 클래스
		},
		loop: false,
		loopFillGroupWithBlank: false,
		ally: {
			enabled: true,
			prevSlideMessage: 'Prev Slide',
			nextSlideMessage: 'Next Slide',
			slideLabelMessage: 'Total {{slidesLength}} Slide {{index}} in the chapter',
		},
		container: {
			a11y: true,
			paginationElement: 'button',
		},
		on: {
			init: function () {
				if (type == 'auto') {
					setSwiperAutoPlay(this, idName);
				}
				setSwiperTab(idName, 'link');
			}, // e : on
		},
	});
	return swiper;
}

// 일정 갯수 이하 일 경우 일반 리스트
function listSwiper(type, idName, pc, pcGap) {
	let autoplay = false;
	if (type === 'auto') {
		autoplay = {
			delay: 10000,
			disableOnInteraction: false,
			pauseOnMouseEnter: true,
		};
	}
	let el = document.getElementById(idName);
	console.log(el.querySelectorAll('.swiper-slide').length);
	if (el.querySelectorAll('.swiper-slide').length > pc) {
		let swiper = new Swiper('#' + idName, {
			slidesPerView: pc,
			spaceBetween: pcGap,
			slidesPerGroup: pc,
			threshold: 50,
			autoplay: autoplay,
			navigation: {
				nextEl: '#' + idName + ' .swiper-next',
				prevEl: '#' + idName + ' .swiper-prev',
			},
			pagination: {
				el: '#' + idName + ' .swiper-pagination',
				type: 'fraction',
			},
			loop: false,
			loopFillGroupWithBlank: false,
			ally: {
				enabled: true,
				prevSlideMessage: 'Prev Slide',
				nextSlideMessage: 'Next Slide',
				slideLabelMessage: 'Total {{slidesLength}} Slide {{index}} in the chapter',
			},
			container: {
				a11y: true,
				paginationElement: 'button',
			},
			on: {
				init: function () {
					if (type == 'auto') {
						setSwiperAutoPlay(this, idName);
					}
					setSwiperTab(idName, 'link');
				}, // e : on
			},
		});
		return swiper;
	} else {
		el.querySelector('.swiper-control-box').style = 'display:none';
		return false;
	}
}

function controlSwiper(type, idName, mobile, mobileGap, tablet, tabletGap, pc, pcGap) {
	let autoplay = false;
	if (type === 'auto') {
		autoplay = {
			delay: 10000,
			disableOnInteraction: false,
			pauseOnMouseEnter: true,
		};
	}
	let swiper = new Swiper('#' + idName, {
		slidesPerView: pc,
		spaceBetween: pcGap,
		slidesPerGroup: pc,
		threshold: 50,
		breakpoints: {
			280: {
				slidesPerView: mobile,
				spaceBetween: mobileGap,
				slidesPerGroup: 1,
			},
			769: {
				slidesPerView: tablet,
				spaceBetween: tabletGap,
				slidesPerGroup: 1,
			},
			1025: {
				slidesPerView: pc,
				spaceBetween: pcGap,
				slidesPerGroup: pc,
			},
		},
		autoplay: autoplay,
		navigation: {
			nextEl: '#' + idName + ' .swiper-next',
			prevEl: '#' + idName + ' .swiper-prev',
		},
		pagination: {
			el: '#' + idName + ' .swiper-pagination',
			type: 'fraction',
		},
		loop: false,
		loopFillGroupWithBlank: false,
		ally: {
			enabled: true,
			prevSlideMessage: 'Prev Slide',
			nextSlideMessage: 'Next Slide',
			slideLabelMessage: 'Total {{slidesLength}} Slide {{index}} in the chapter',
		},
		container: {
			a11y: true,
			paginationElement: 'button',
		},
		on: {
			init: function () {
				if (type == 'auto') {
					setSwiperAutoPlay(this, idName);
				}
				setSwiperTab(idName, 'link');
			}, // e : on
		},
	});
	return swiper;
}

function scrollswiper(idName, mobile, mobileGap, tablet, tabletGap, pc, pcGap) {
	let autoplay = false;
	autoplay = {
		delay: 3000,
		disableOnInteraction: false,
	};
	let swiper = new Swiper('#' + idName, {
		slidesPerView: pc,
		spaceBetween: pcGap,
		slidesPerGroup: pc,
		threshold: 50,
		breakpoints: {
			280: {
				slidesPerView: mobile,
				spaceBetween: mobileGap,
				slidesPerGroup: 1,
			},
			769: {
				slidesPerView: tablet,
				spaceBetween: tabletGap,
				slidesPerGroup: tablet,
			},
			1025: {
				slidesPerView: pc,
				spaceBetween: pcGap,
				slidesPerGroup: pc,
			},
		},
		autoplay: autoplay,
		navigation: {
			nextEl: '#' + idName + ' .swiper-next',
			prevEl: '#' + idName + ' .swiper-prev',
		},
		scrollbar: {
			el: '#' + idName + ' .swiper-scrollbar',
		},
		pagination: {
			el: '#' + idName + ' .swiper-pagination',
			type: 'fraction',
		},
		loop: false,
		loopFillGroupWithBlank: false,
		ally: {
			enabled: true,
			prevSlideMessage: 'Prev Slide',
			nextSlideMessage: 'Next Slide',
			slideLabelMessage: 'Total {{slidesLength}} Slide {{index}} in the chapter',
		},
		container: {
			a11y: true,
			paginationElement: 'button',
		},
		on: {
			init: function () {
				setSwiperAutoPlay(this, idName);

				setSwiperTab(idName, 'link');
			}, // e : on
		},
	});
	return swiper;
}

function zoomSwiper(idName, mobile, mobileGap, tablet, tabletGap, pc, pcGap) {
	let zoomSwiper = new Swiper('#' + idName, {
		slidesPerView: pc,
		spaceBetween: pcGap,
		observer: true,
		observeParents: true,
		breakpoints: {
			280: {
				slidesPerView: mobile,
				spaceBetween: mobileGap,
			},
			768: {
				slidesPerView: tablet,
				spaceBetween: tabletGap,
			},
			1024: {
				slidesPerView: pc,
				spaceBetween: pcGap,
			},
		},
		navigation: {
			nextEl: '#' + idName + ' .swiper-next',
			prevEl: '#' + idName + ' .swiper-prev',
		},
		loop: false,
		loopFillGroupWithBlank: false,
		ally: {
			enabled: true,
			prevSlideMessage: 'Prev Slide',
			nextSlideMessage: 'Next Slide',
			slideLabelMessage: 'Total {{slidesLength}} Slide {{index}} in the chapter',
		},
		container: {
			a11y: true,
		},
		on: {
			init: function () {
				setSwiperTab(idName, 'zoom');
			}, // init
		}, // e : on
	});

	// zoom image change
	let big_img = document.querySelector('.images-zoom').querySelector('img');
	let big_iframe = document.querySelector('.images-zoom').querySelector('iframe');
	let click_slide_item = document.querySelectorAll('#' + idName + ' .swiper-slide');
	let this_target, this_link, this_type;

	function allActiveRemove() {
		let active_item = document.querySelectorAll('#' + idName + ' .slide-active');

		active_item.forEach(event => {
			event.classList.remove('slide-active');
		});
	}

	// zoom swiper 영상 및 이미지별 이벤트 함수
	function typeCheck(target, target_link, target_type) {
		target.classList.add('slide-active');

		if (target_type == 'img') {
			big_img.style.display = 'block';
			big_iframe.style.display = 'none';
			big_img.setAttribute('src', target_link);
		} else if (target_type == 'vr' || target_type == 'ar' || target_type == 'youtube') {
			big_img.style.display = 'none';
			big_iframe.style.display = 'block';

			let link_id = target_link.split('/');
			let video_link = 'https://www.youtube.com/embed/' + link_id[4];

			if (target_type == 'vr') {
				big_iframe.setAttribute('src', video_link);
			} else if (target_type == 'ar') {
				big_iframe.setAttribute('src', video_link);
			} else if (target_type == 'youtube') {
				big_iframe.setAttribute('src', video_link);
			}
		}
	}

	// swiper click change event
	click_slide_item.forEach(event => {
		event.addEventListener('click', () => {
			this_link = event.children[0].children[0].getAttribute('src');
			this_type = event.children[0].getAttribute('data-type');
			this_target = event.children[0].children[0];
			allActiveRemove();
			typeCheck(this_target, this_link, this_type);
		});
	});

	document.querySelector('#' + idName + ' .swiper-next').addEventListener('click', () => {
		noChangeActive('next');
	});

	document.querySelector('#' + idName + ' .swiper-prev').addEventListener('click', () => {
		noChangeActive('prev');
	});

	// zoom swiper arrow event
	function noChangeActive(target) {
		let index_main, new_index;

		index_main = document.querySelector('#' + idName + ' .swiper-slide-active');

		if (target === 'next') {
			new_index = document.querySelector('#' + idName + ' .swiper-slide-next');
		} else if (target === 'prev') {
			new_index = document.querySelector('#' + idName + ' .swiper-slide-prev');
		}

		allActiveRemove();

		new_index.children[0].children[0].classList.remove('slide-active');

		this_target = index_main.children[0].children[0];
		this_link = index_main.children[0].children[0].getAttribute('src');
		this_type = index_main.children[0].getAttribute('data-type');
		typeCheck(this_target, this_link, this_type);
	}

	const img_lens = document.querySelector('.images-zoom-lens');
	const img_copy = document.querySelector('.images-zoom-copy');

	// big img zoom event
	function imageZoom() {
		let img, lens, result, cx, cy; // 이미지, 길이, 반환, x, y 값
		img = big_img; // 이미지 요소
		result = img_copy; // 반환 요소
		lens = img_lens; // 줌 렌즈 요소
		cx = result.offsetWidth / lens.offsetWidth; // 반환 요소 넓이 / 렌즈 요소 넓이
		cy = result.offsetHeight / lens.offsetHeight; // 반환 요소 높이  / 렌즈 요소 높이
		result.style.backgroundImage = "url('" + img.src + "')"; // 백그라운드 이미지로 이미지 복사
		//result.style.backgroundSize = img.width * 3 + 'px ' + img.height * 3 + 'px'; // 사이즈 복사
		result.style.backgroundSize = '200% 200%'; // 사이즈 복사
		lens.addEventListener('mousemove', moveLens);
		img.addEventListener('mousemove', moveLens);
		lens.addEventListener('touchmove', moveLens);
		img.addEventListener('touchmove', moveLens);

		function moveLens(e) {
			let pos, x, y;
			e.preventDefault(); // 새로고침 및 이동 막기
			pos = getCursorPos(e); // e 의 마우스 커서의 위치를 가져온다
			x = pos.x - lens.offsetWidth / 2; // 렌즈의 요소 넒이 / 2
			y = pos.y - lens.offsetHeight / 2; // 렌즈의 요소 높이 / 2
			// x  > 이미지 넒이 - 렌즈 요소 넒이 { x = 이미지 넒이 - 렌즈 요소 넒이 }
			if (x > img.width - lens.offsetWidth) {
				x = img.width - lens.offsetWidth;
			}
			if (x < 0) {
				x = 0;
			}
			// y > 이미지 높이 - 렌즈 요소 높이 { y = 이미지 높이 - 렌즈 요소 높이 }
			if (y > img.height - lens.offsetHeight) {
				y = img.height - lens.offsetHeight;
			}
			if (y < 0) {
				y = 0;
			}
			lens.style.left = x + 'px';
			lens.style.top = y + 'px';
			result.style.backgroundPosition = '-' + x * 1.5 + 'px -' + y * 1.5 + 'px';
		}
		// 마우스 커서 따라 다니기
		function getCursorPos(e) {
			let a,
				x = 0,
				y = 0;
			e = e || window.event; // 웹에서 현재 처리하는 이벤트를 반환한다
			a = img.getBoundingClientRect(); // 이미지의 엘레멘트 크기와 뷰포트 상대 위치 정보 제공
			x = e.pageX - a.left;
			y = e.pageY - a.top;
			x = x - window.pageXOffset;
			y = y - window.pageYOffset;
			return {x: x, y: y};
		}
	}

	big_img.addEventListener('mouseenter', () => {
		let filter = 'win16|win32|win64|mac|macintel';

		if (0 < filter.indexOf(navigator.platform.toLowerCase())) {
			imageZoom();
			img_lens.style.visibility = 'visible';
			img_copy.style.visibility = 'visible';
			img_copy.style.zIndex = '1';
		}
	});

	document.querySelector('.images-zoom').addEventListener('mouseleave', () => {
		img_lens.style.visibility = 'hidden';
		img_copy.style.visibility = 'hidden';
		img_copy.style.zIndex = '0';
	});
}

function overlaySwiper(modalName, idName, mobile, mobileGap, tablet, tabletGap, pc, pcGap) {
	let swiper = new Swiper('#' + idName, {
		slidesPerView: pc,
		spaceBetween: pcGap,
		slidesPerGroup: pc,
		threshold: 50,
		breakpoints: {
			280: {
				slidesPerView: mobile,
				spaceBetween: mobileGap,
				slidesPerGroup: mobile,
			},
			768: {
				slidesPerView: tablet,
				spaceBetween: tabletGap,
				slidesPerGroup: tablet,
			},
			1024: {
				slidesPerView: pc,
				spaceBetween: pcGap,
				slidesPerGroup: pc,
			},
		},
		navigation: {
			nextEl: '#' + idName + ' .swiper-next',
			prevEl: '#' + idName + ' .swiper-prev',
		},
		loop: false,
		loopFillGroupWithBlank: false,
		pagination: {
			el: '#' + idName + ' .swiper-pagination', // 페이징 태그 클래스
		},
		ally: {
			enabled: true,
			prevSlideMessage: 'Prev Slide',
			nextSlideMessage: 'Next Slide',
			slideLabelMessage: 'Total {{slidesLength}} Slide {{index}} in the chapter',
		},
		container: {
			a11y: true,
			paginationElement: 'button',
		},
		on: {
			init: function () {
				setSwiperTab(idName, 'overlay');
			},
		},
	});

	// 전체 modal 버튼에 탭 인덱스 추가
	const modalBtn = document.querySelectorAll('.modal-button');

	modalBtn.forEach(event => {
		event.setAttribute('tabindex', '0');
	});

	const modalClose = document.querySelector('.modal-card-close');
	modalClose.addEventListener('click', () => {
		closeModal();
	});

	function closeModal() {
		document.getElementById(modalName + '-modal').classList.toggle('is-active');
		document.querySelector('html').classList.toggle('is-clipped');
	}

	// 오버레이에 키보드 접근시 팝업 띄우기
	const overSwiper = document.getElementById(idName);
	const modalSWiperSlide = overSwiper.querySelectorAll('.swiper-slide:not(.swiper-slide-duplicate)');
	modalSWiperSlide.forEach(event => {
		event.setAttribute('tabindex', '0');
		event.addEventListener('focus', () => {
			const imgOverlay = event.childNodes[2];
			const qiewViewBtn = imgOverlay.childNodes[1].childNodes[3];

			imgOverlay.style.display = 'block';
			qiewViewBtn.addEventListener('keydown', keyEvent => {
				if (keyEvent.keyCode == 13) {
					document.getElementById(modalName + '-modal').setAttribute('overlayIndex', event.getAttribute('data-swiper-slide-index'));
					const target = qiewViewBtn.getAttribute('data-target');
					document.getElementById(target).classList.toggle('is-active');
					document.querySelector('html').classList.toggle('is-clipped');
					document.querySelector('.modal-img').focus();
				}
			});
		});
	});

	modalClose.addEventListener('keydown', keyEvent => {
		if (keyEvent.keyCode == 13) {
			modalSWiperSlide.forEach(event => {
				if (event.getAttribute('data-swiper-slide-index') == document.getElementById(modalName + '-modal').getAttribute('overlayIndex')) {
					event.childNodes[2].childNodes[1].childNodes[3].focus();
				}
			});

			closeModal();
		} else if (keyEvent.keyCode == 9) {
			keyEvent.preventDefault();
		}
	});
}

function numberSwiper(idName, mobile, mobileGap, tablet, tabletGap, pc, pcGap) {
	let swiper = new Swiper('#' + idName, {
		slidesPerView: pc,
		spaceBetween: pcGap,
		slidesPerGroup: pc,
		threshold: 50,
		breakpoints: {
			280: {
				slidesPerView: mobile,
				spaceBetween: mobileGap,
				slidesPerGroup: mobile,
			},
			768: {
				slidesPerView: tablet,
				spaceBetween: tabletGap,
				slidesPerGroup: tablet,
			},
			1024: {
				slidesPerView: pc,
				spaceBetween: pcGap,
				slidesPerGroup: pc,
			},
		},
		navigation: {
			nextEl: '#' + idName + ' .swiper-next',
			prevEl: '#' + idName + ' .swiper-prev',
		},
		autoHeight: true,
		loop: false,
		loopFillGroupWithBlank: false,
		pagination: {
			el: '#' + idName + ' .swiper-pagination', // 페이징 태그 클래스
			type: 'fraction',
		},
		a11y: {
			enabled: true,
			prevSlideMessage: 'Prev Slide',
			nextSlideMessage: 'Next Slide',
			slideLabelMessage: 'Total {{slidesLength}} Slide {{index}} in the chapter.',
		},
		container: {
			a11y: true,
			paginationElement: 'button',
		},
		on: {
			init: function () {
				setSwiperTab(idName, 'link');
			}, // init
		}, // e : on
	});

	return swiper;
}

// gellerySwiper('swiper-gallery', 4, 15, 4, 15, 4, 15);
function gellerySwiper(idName, mobile, mobileGap, tablet, tabletGap, pc, pcGap) {
	let thumbSwiper = new Swiper('#' + idName + ' .type-thumb', {
		slidesPerView: pc,
		spaceBetween: pcGap,
		// observer: true,
		// observeParents: true,
		// loop: true,
		// loopFillGroupWithBlank: false,
		// allowTouchMove: true,
		// simulateTouch: false,
		// freeMode: true,
		// watchSlidesProgress: true,
		threshold: 10,
		breakpoints: {
			280: {
				slidesPerView: mobile,
				spaceBetween: mobileGap,
			},
			769: {
				slidesPerView: tablet,
				spaceBetween: tabletGap,
			},
			1024: {
				slidesPerView: pc,
				spaceBetween: pcGap,
			},
		},
	});
	let viewSwiper = new Swiper('#' + idName + ' .type-view', {
		slideActiveClass: 'swiper-slide-view-active',
		spaceBetween: pcGap,
		navigation: {
			nextEl: '#' + idName + ' .swiper-next',
			prevEl: '#' + idName + ' .swiper-prev',
		},
		thumbs: {
			swiper: thumbSwiper,
		},
		on: {
			init: function () {
				let zoomCopy = document.querySelector('.images-zoom-copy');
				let zoomItem = document.querySelectorAll('.images-zoom');
				// zoomItem.forEach(function (item, index) {
				// 	let zoomImg = item.querySelector('img');
				// 	let zoomLens = item.querySelector('.images-zoom-lens');
				// 	imageZoom(zoomImg, zoomLens, zoomCopy);
				// });

				let iframeItem = document.querySelectorAll('.type-view .swiper-slide .iframe');
				iframeItem.forEach(item => {
					item.parentElement.classList.add('is-mov');
				});
			},
		},
		breakpoints: {
			// 280: {
			// 	slidesPerView: mobile,
			// 	spaceBetween: mobileGap,
			// },
			769: {
				slidesPerView: 1,
				spaceBetween: mobileGap,
			},
		},
	});
	viewSwiper.on('activeIndexChange', function () {
		// youtube player 일시정지
		let youtbePlayer = document.querySelectorAll('.iframe .iframe-player');
		for (i = 0; i < youtbePlayer.length; i++) {
			youtbePlayer[i].contentWindow.postMessage('{"event":"command", "func":"pauseVideo","args":""}', '*');
		}
	});

	
}
// big img zoom event
function imageZoom(zoomImg, zoomLens, zoomCopy) {
	let img, lens, result, cx, cy; // 이미지, 길이, 반환, x, y 값
	imgWrap = zoomImg.parentElement;
	img = zoomImg; // 이미지 요소
	result = zoomCopy; // 반환 요소
	lens = zoomLens; // 줌 렌즈 요소
	cx = result.offsetWidth / lens.offsetWidth; // 반환 요소 넓이 / 렌즈 요소 넓이
	cy = result.offsetHeight / lens.offsetHeight; // 반환 요소 높이  / 렌즈 요소 높이
	// result.style.backgroundImage = "url('" + img.src + "')"; // 백그라운드 이미지로 이미지 복사
	// result.style.backgroundSize = img.width * 3 + 'px ' + img.height * 3 + 'px'; // 사이즈 복사
	// console.log(img.src.split('resources')[1]);
	result.style.backgroundSize = '200% 200%'; // 사이즈 복사
	lens.addEventListener('mousemove', moveLens);
	img.addEventListener('mousemove', moveLens);
	lens.addEventListener('touchmove', moveLens);
	img.addEventListener('touchmove', moveLens);

	function moveLens(e) {
		let pos, x, y;
		// e.preventDefault(); // 새로고침 및 이동 막기
		if (e.cancelable) e.preventDefault();
		pos = getCursorPos(e); // e 의 마우스 커서의 위치를 가져온다
		x = pos.x - lens.offsetWidth / 2; // 렌즈의 요소 넒이 / 2
		y = pos.y - lens.offsetHeight / 2; // 렌즈의 요소 높이 / 2
		// x  > 이미지 넒이 - 렌즈 요소 넒이 { x = 이미지 넒이 - 렌즈 요소 넒이 }
		if (x > img.width - lens.offsetWidth) {
			x = img.width - lens.offsetWidth;
		}
		if (x < 0) {
			x = 0;
		}
		// y > 이미지 높이 - 렌즈 요소 높이 { y = 이미지 높이 - 렌즈 요소 높이 }
		if (y > img.height - lens.offsetHeight) {
			y = img.height - lens.offsetHeight;
		}
		if (y < 0) {
			y = 0;
		}
		lens.style.left = x + 'px';
		lens.style.top = y + 'px';
		result.style.backgroundPosition = '-' + x * 1.5 + 'px -' + y * 1.5 + 'px';
	}
	// 마우스 커서 따라 다니기
	function getCursorPos(e) {
		let a,
			x = 0,
			y = 0;
		e = e || window.event; // 웹에서 현재 처리하는 이벤트를 반환한다
		a = img.getBoundingClientRect(); // 이미지의 엘레멘트 크기와 뷰포트 상대 위치 정보 제공
		x = e.pageX - a.left;
		y = e.pageY - a.top;
		x = x - window.pageXOffset;
		y = y - window.pageYOffset;
		return {x: x, y: y};
	}

	img.addEventListener('mouseenter', e => {
		// result.style.backgroundImage = "url('../../resources" + img.src.split('resources')[1] + "')"; // 백그라운드 이미지로 이미지 복사
		result.style.backgroundImage = "url('" + img.src + "')"; // 백그라운드 이미지로 이미지 복사
		let filter = 'win16|win32|win64|mac|macintel';

		if (0 < filter.indexOf(navigator.platform.toLowerCase())) {
			// imageZoom();
			zoomLens.style.visibility = 'visible';
			zoomCopy.style.visibility = 'visible';
			zoomCopy.style.zIndex = '2';
		}
	});

	imgWrap.addEventListener('mouseleave', () => {
		zoomLens.style.visibility = 'hidden';
		zoomCopy.style.visibility = 'hidden';
		zoomCopy.style.zIndex = '0';
	});
}
function eachPerSwiper(idName, mobile, mobileGap, tablet, tabletGap, pc, pcGap) {
	let swiper = new Swiper('#' + idName, {
		slidesPerView: pc,
		spaceBetween: pcGap,
		//threshold: 50,
		observer: true,
		observeParents: true,
		breakpoints: {
			280: {
				slidesPerView: mobile,
				spaceBetween: mobileGap,
				touchRatio: 0.5,
				resistance: true,
				//freeMode : true, 
				slidesPerGroup: 1,
			},
			769: {
				slidesPerView: tablet,
				spaceBetween: tabletGap,
				slidesPerGroup: 1,
			},
			1025: {
				slidesPerView: pc,
				spaceBetween: pcGap,
			},
		},
		navigation: {
			nextEl: '#' + idName + ' .swiper-next',
			prevEl: '#' + idName + ' .swiper-prev',
		},
		pagination: {
			el: '#' + idName + ' .swiper-pagination',
			type: 'fraction',
		},
		loop: false,
		loopFillGroupWithBlank: false,
		ally: {
			enabled: true,
			prevSlideMessage: 'Prev Slide',
			nextSlideMessage: 'Next Slide',
			slideLabelMessage: 'Total {{slidesLength}} Slide {{index}} in the chapter',
		},
		container: {
			a11y: true,
			paginationElement: 'button',
		},
		on: {
			init: function () {
				setSwiperTab(idName, 'link');
			}, // e : on
		},
	});
	return swiper;
}