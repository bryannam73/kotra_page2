'use strict';

document.addEventListener('DOMContentLoaded', function () {
	// 상품 리스트페이지 favorite toast + is-active 클래스 부여
	if (document.querySelectorAll('.button-list-favorite')) {
		let elVar = document.querySelectorAll('.button-list-favorite');
		elVar.forEach(target => {
			target.addEventListener('click', function (event) {
				let icons = target.querySelectorAll('.mobile-icon')[0];
				event.stopPropagation();
				if (target.classList.contains('is-active')) {
					target.classList.remove('is-active');
					displayToast('Bottom Center', 'is-danger', 'Remove to Favorites.');
					// icons.classList.remove('ico-heart-on');
					// icons.classList.add('ico-heart-off');
				} else {
					target.classList.add('is-active');
					displayToast('Bottom Center', 'is-success', 'Success! Add to Favorites.');
					// icons.classList.remove('ico-heart-off');
					// icons.classList.add('ico-heart-on');
				}
			});
		});
	}

	// 기업상세 즐겨찾기
	// heart click event
	if (document.querySelectorAll('.favorite-btn')) {
		let heartBtn = document.querySelectorAll('.favorite-btn');
		heartBtn.forEach(target => {
			target.addEventListener('click', () => {
				target.classList.toggle('is-active');
			});
		});
	}
});
