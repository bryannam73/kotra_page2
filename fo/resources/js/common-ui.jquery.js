'use strict';

// S: Dynamic Tab + Content -- jQuery
$(function () {
	// show/hide 처리
	$(document).on('click', '[role="show"]', function (event) {
		event = event || window.event;
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);

		// 선택된 탭 활성화
		$('#' + $(this).attr('aria-controls'))
			.removeAttr('hidden')
			.attr({ tabindex: '0', 'aria-hidden': 'false' })
			.addClass('is-active')
			// 기존 탭 패널 비활성화
			.siblings('.showpanel')
			.attr({ tabindex: '-1', 'aria-hidden': 'true', hidden: '' })
			.removeClass('is-active');
	});

	// let tabFocus = 0;
	$(document).on('click', '[role="tab"]', function (event) {
		event = event || window.event;
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);

		// 선택된 탭 활성화
		tabActivate($(this));
		// 탭 하위에 탭이 존재하는 경우 초기화 처리
		var $thisChildren = $('#' + $(this).attr('aria-controls')).find('[role="tablist"]');
		if ($thisChildren.length) {
			tabActivate($thisChildren.children().eq(0).children('[role="tab"]'));
			$('#' + $(this).attr('aria-controls')).removeAttr('tabindex');
		}
	});

	$(document).on('keydown', '[role="tab"]', function (event) {
		event = event || window.event;
		var keycode = event.keyCode || event.which;
		var tabsLength = $(this).children('li').length;
		if (keycode === 39 || keycode === 37) {
			var firstEl = $(this).closest('[role="tablist"]').children('li').eq(0).children('[role="tab"]');
			var lastEl = $(this)
				.closest('[role="tablist"]')
				.children('li')
				.eq(tabsLength - 1)
				.children('[role="tab"]');
			$(this).parent('li').addClass('');
			if (keycode === 39) {
				$(this).parent('li').next().children('[role="tab"]').focus();
				if (!$(this).parent('li').next().length) {
					firstEl.focus();
				}
			} else if (keycode === 37) {
				$(this).parent('li').prev().children('[role="tab"]').focus();
				if (!$(this).parent('li').prev().length) {
					lastEl.focus();
				}
			}
		}
	});

	// 상품유닛용 툴팁 선언을 위한 스크립트 : 200701
	if ($('[data-toggle="tooltip"]').length) {
		$('[data-toggle="tooltip"]').tooltip();
	}

	// Collapse 처리
	// 초기화
	$('.is-collapsible').each(function () {
		if ($(this).hasClass('is-active')) {
			$(this).attr({ 'aria-expanded': 'true' });
		} else {
			$(this).attr({ 'aria-expanded': 'false' }).css({ display: 'none' });
		}
	});
	$('[data-action="collapse"]').each(function () {
		if ($($(this).attr('href')).hasClass('is-active')) {
			$(this).addClass('is-active');
		} else {
			$(this).removeClass('is-active');
		}
	});
	// collapse 작동
	$(document).on('click', '[data-action="collapse"]', function (event) {
		event = event || window.event;
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		var $target = $($(this).attr('href'));
		// 아코디언 형태인 경우
		if ($target.data('parent') !== undefined && $target.data('parent') != '') {
			var accordionParent = $target.data('parent');

			if ($(this).hasClass('is-active')) {
				$target
					.closest('#' + accordionParent)
					.find('.is-collapsible')
					.each(function () {
						if ($(this).hasClass('is-active')) {
							$(this).attr({ 'aria-expanded': 'false' }).removeClass('is-active').slideUp('fast');
						}
					});
				$(this).removeClass('is-active');
				$target.attr({ 'aria-expanded': 'false' }).removeClass('is-active').slideUp('fast');
			} else {
				$target
					.closest('#' + accordionParent)
					.find('[data-action="collapse"]')
					.removeClass('is-active');
				$target
					.closest('#' + accordionParent)
					.find('.is-collapsible')
					.removeClass('is-active')
					.delay(10)
					.slideUp('fast');
				$(this).addClass('is-active');
				$target.attr({ 'aria-expanded': 'true' }).addClass('is-active').stop().slideDown('fast');
			}
		}
		// 일반 collapse 인 경우
		else {
			if ($(this).hasClass('is-active')) {
				$(this).removeClass('is-active');
				$target.attr({ 'aria-expanded': 'false' }).removeClass('is-active').slideUp('fast');
			} else {
				$(this).addClass('is-active');
				$target.attr({ 'aria-expanded': 'true' }).addClass('is-active').stop().slideDown('fast');
			}
		}
	});
});

// 장바구니 수량 증감
$(document)
	.on('click', '.spinner-box:not(.spinner-box__not) .spinner-box__plus', function (e) {
		var countVal = parseInt($(this).closest('.spinner-box').find('input[type=number]').val());
		var toCountVal = countVal + 1;
		if (toCountVal > 99) {
			// alert('구매수량은 100개 이상이 될 수 없습니다.');
			$(this).closest('.spinner-box').find('input[type=number]').val(99);
		} else if (toCountVal <= 1) {
			// alert('구매수량은 1개 이상이어야 합니다.');
			$(this).closest('.spinner-box').find('input[type=number]').val(1);
		} else {
			$(this).closest('.spinner-box').find('input[type=number]').val(toCountVal);
		}
	})
	.on('click', '.spinner-box:not(.spinner-box__not) .spinner-box__minus', function (e) {
		var countVal = parseInt($(this).closest('.spinner-box').find('input[type=number]').val());
		var toCountVal = countVal - 1;
		if (toCountVal > 99) {
			// alert('구매수량은 100개 이상이 될 수 없습니다.');
			$(this).closest('.spinner-box').find('input[type=number]').val(99);
		} else if (toCountVal <= 1) {
			// alert('구매수량은 1개 이상이어야 합니다.');
			$(this).closest('.spinner-box').find('input[type=number]').val(1);
		} else {
			$(this).closest('.spinner-box').find('input[type=number]').val(toCountVal);
		}
	});

function tabActivate(thisEl) {
	thisEl.closest('[role="tablist"]').children().children('[aria-selected="true"]').attr({
		tabindex: '-1',
		'aria-selected': 'false',
	});
	thisEl.parent().addClass('is-active').siblings().removeClass('is-active');
	thisEl.attr({
		tabindex: '0',
		'aria-selected': 'true',
	});
	// 연관된 탭 패널 활성화
	$('#' + thisEl.attr('aria-controls'))
		.removeAttr('hidden')
		.attr('tabindex', '0')
		.addClass('is-active')		// 기존 탭 패널 비활성화
		.siblings('.tabpanel')
		.attr({ 'tabindex': '-1', 'hidden': '' })
		.removeClass('is-active');
	// console.log($('#' + thisEl.attr('aria-controls')));
}
// E : Dynamic Tab + Content -- jQuery

// S : Dropdowns
// Functions

function getAll(selector) {
	return Array.prototype.slice.call(document.querySelectorAll(selector), 0);
}

var $dropdowns = getAll('.dropdown:not(.is-hoverable)');
$(document)
	.on('click', function (e) {
		const className = e.target.className;
		if (!className.includes('modal-background') && !className.includes('modal-close') && !className.includes('delete') && !className.includes('button-modal-close')) {
			if (!$(e.target).closest('.dropdown').length) {
				$('.dropdown.is-active').removeClass('is-active');
			}
		}
	})
	.on('mouseenter focus', '.dropdown-trigger .button', function (e) {
		$('.dropdown').removeClass('is-hover');
		$(this).closest('.dropdown').addClass('is-hover');
	})
	.on('mouseleave', '.dropdown-trigger .button', function (e) {
		$(this).closest('.dropdown').removeClass('is-hover');
	})
	.on('click', '.dropdown-trigger .button', function (e) {
		e.preventDefault();
		$('.dropdown:not(.is-hover)').removeClass('is-active');
		if ($(this).closest('.dropdown').hasClass('is-active')) {
			$(this).closest('.dropdown').removeClass('is-active');
		} else {
			$(this).closest('.dropdown').addClass('is-active');
		}
	})
	.on('click', '.dropdown-item', function (e) {
		// 드롭다운 관련 추가 : 드롭다운 dropdown 아이템 아이콘 관련 이벤트 처리
		if (!$(this).closest('.header-user-dropdown').length) {
			if ($(this).hasClass('is-icon')) {
				$(this).addClass('is-active').siblings().removeClass('is-active');
				$(this).closest('.dropdown').find('[aria-controls="dropdown-menu"]').children('span').eq(0).empty().append($(this).children().clone());
				$(this).closest('.dropdown').find('[aria-controls="dropdown-menu"]').children('span').eq(0).append($(this).text());
			} else {
				$(this).addClass('is-active').siblings().removeClass('is-active');
				$(this).closest('.dropdown').find('[aria-controls="dropdown-menu"]').children('span').eq(0).text($(this).text());
			}
			var dropdownWidth = $(this).outerWidth();
			if ($('.header-search__dropdown').length) {
				$(this).closest('.header-search__dropdown').css('min-width', dropdownWidth).find('.dropdown').css('width', 'auto');
			}
			$('.dropdown.is-active').removeClass('is-active');
		}

	});

function closeDropdowns() {
	$dropdowns.forEach(function ($el) {
		$el.classList.remove('is-active');
	});
}
// E : Dropdowns
// Modals : 20200602
// Modal Open
// modal 액션시 tab-index 추가 및 포커스 새 레이어로 이동 : 200722
$(document).on('click', '.modal-button', function (e) {
	e.preventDefault();
	var target = '#' + $(this).data('target');
	$('html').addClass('is-clipped');
	$(target).addClass('is-active').attr('tabindex', 0).focus();
});
// modal 상태에서 띄워주는 modal 호출 버튼 : 200710
$(document).on('click', '.modal-button-this', function (e) {
	e.preventDefault();
	var target = '#' + $(this).data('target');
	$(target).addClass('is-active').attr('tabindex', 0).focus();
});
// Modal Close
$(document).on('click', '.modal-background, .modal-close, .modal-card-head .delete, .modal .button-modal-close', function (e) {
	closeModals();
});
// modal 상태에서 띄워져 있는 modal 닫기 버튼 : 200710
$(document).on('click', '.modal-background-this, .modal-close-this', function (e) {
	$(this).closest('.modal').removeClass('is-active');
});
function closeModals() {
	$('html').removeClass('is-clipped');
	$(document).find('.modal').removeClass('is-active');
}
$(document).on('keydown', function (event) {
	var e = event || window.event;
	if (e.keyCode === 27) {
		closeModals();
		// closeDropdowns();
		$('.dropdown.is-active').removeClass('is-active');
	}
});
document.addEventListener('keydown', function (event) {
	var e = event || window.event;
	if (e.keyCode === 27) {
		closeModals();
		closeDropdowns();
	}
});

// document.addEventListener('DOMContentLoaded', function () {
// 	function scrollToTop() {
// 		window.scrollTo(0, 0);
// 	}
// 	// Utils
// 	function removeFromArray(array, value) {
// 		if (array.includes(value)) {
// 			var value_index = array.indexOf(value);
// 			array.splice(value_index, 1);
// 		}

// 		return array;
// 	}
// 	Array.prototype.diff = function (a) {
// 		return this.filter(function (i) {
// 			return a.indexOf(i) < 0;
// 		});
// 	};
// });

// S : 체크박스 그룹 전체 선택
// 체크박스 그룹내의 check item 의 checked 상태 체크
function checkboxEach($el) {
	var is_checked = true;
	$el.closest('.check__group')
		.find('input[type=checkbox]')
		.each(function () {
			if (!$(this).hasClass('check__all') && !$(this).attr('disabled')) {
				is_checked = is_checked && $(this).is(':checked');
			}
		});
	return is_checked;
}
$(document)
	.on('click', '.check__group .check__all', function () {
		// 체크박스 전체항목 선택시 그룹내의 모든 체크 선택
		$(this).closest('.check__group').find('input:not([disabled])').prop('checked', $(this).is(':checked'));
	})
	.on('click', '.check__group input[type=checkbox]', function () {
		// 체크박스 개별 선택시 그룹내의 체크박스 확인
		if (!$(this).hasClass('check__all')) {
			var is_checked = checkboxEach($(this).closest('.check__group').find('.check__all'));
			$(this).closest('.check__group').find('.check__all').prop('checked', is_checked);
		}
	});
// E : 체크박스 그룹 전체 선택


// mobile header fixed (ios scroll bounce issue)
// mobile footer app bar
var lastScrollTop = 0;
function upDownScroll() {
	// var lastScrollTop = 0;
	var scrollBodyHeight = $('body').prop('scrollHeight');
	var winHeight = $(window).height();

	var st = $(window).scrollTop();
	
	if (st <= 150) {
		// $('.button-scroll-top').hide();
		$('.button-scroll-top').removeClass('is-active');
	} else {
		if (st > lastScrollTop) {
			// downscroll code
			// $('.button-scroll-top').show();
			$('.button-scroll-top').addClass('is-active');
		} else {
			// upscroll code
			// $('.button-scroll-top').hide();
			$('.button-scroll-top').removeClass('is-active');
		}
	}
	lastScrollTop = st;
}
$(document).ready(function () {
	upDownScroll();
});
$(window).on('scroll', function (e) {
	upDownScroll();
});


// 맨 위로 이동 클릭
$(document).on('click', '.button-scroll-top', function (e) {
	e.preventDefault();
	$('body, html').animate({ scrollTop: 0 });
});

// s: menu-ui 전체 MENU 클릭시 javascript
$(function () { 
	$(document).on('click', '.menu-open', function () {
		$('body').addClass('__menu-open');
	});
	$(document).on('click', '.menu-close', function () {
		$('body').removeClass('__menu-open');
	});
	$(document).on('click', '.button-header-user', function () {
		if ($('.header-user').hasClass('is-active')) {
			$('.header-user').removeClass('is-active');
		} else {
			$('.header-user').addClass('is-active');
		}
	});
})

// Sub : content-section 펼침 닫힘
$(document).on('click', '.content-section-header', function () {
	if ($('.content-section-header').hasClass('is-active')) {
		$('.content-section-header').removeClass('is-active');
	} else {
		$('.content-section-header').addClass('is-active');
	}
});
document.addEventListener('DOMContentLoaded', function () {
	/*
	if (!!document.querySelector('#header')) { 
		document.querySelector('#header').addEventListener('click', function (e) {
			console.log(e.target.className);
			if (e.target.className.includes('menu-open')) { 
				const menuEl = document.getElementsByClassName('menu-open')[0];
				const bodyEl = document.querySelector('body');
				bodyEl.classList.add('__menu-open');
			}
			// if (menuEl.className.includes('is-active')) {
			// 	bodyEl.classList.remove('__menu-open');
			// 	menuEl.classList.remove('is-active');
			// } else {
			// 	bodyEl.classList.add('__menu-open');
			// 	menuEl.classList.add('is-active');
			// }
		});
		
		// document.getElementsByClassName('menu-close')[0].addEventListener('click', function () {
		// 	const menuEl = document.getElementsByClassName('menu-close')[0];
		// 	const bodyEl = document.querySelector('body');
		// 	bodyEl.classList.remove('__menu-open');
		// });
	}
	if (!!document.querySelector('.button-header-user')) { 
		document.querySelector('.button-header-user').addEventListener('click', function () {
			const huserEl = document.querySelector('.header-user');
			if (huserEl.classList.contains('is-active')) {
				huserEl.classList.remove('is-active');
			} else {
				huserEl.classList.add('is-active');
			}
		});
	}
	*/

	// 콘텐츠 영역 공통 : contents-search 섹션 펼침/닫힘
	if (!!document.querySelector('.button-content-search-collapse')) { 
		document.querySelector('.button-content-search-collapse').addEventListener('click', function () {
			const cntEl = document.querySelector('.content-search');
			if (cntEl.classList.contains('is-active')) {
				cntEl.classList.remove('is-active');
			} else {
				cntEl.classList.add('is-active');
			}
		});
	}

	// 대시보드 : 위젯 헤더 타이틀/바디 펼침 닫힘
	if (!!document.querySelector('.main-widget')) { 
		const elHeader = document.querySelectorAll('.main-widget-header');

		elHeader.forEach(el => el.addEventListener('click', event => {
			if (event.target.classList.contains('is-active')) {
				event.target.classList.remove('is-active');
			} else {
				event.target.classList.add('is-active');
			}
		}));
	}


	/*
	const lnbMenu = document.querySelector('.lnb-menulist');
	const lnbMenuLink = lnbMenu.querySelectorAll('a');

	[].forEach.call(lnbMenuLink, function (link) {
		link.addEventListener("click", click, false);
	});
	function click(e) {
		console.log(this.innerHTML);
		try {
			console.log(this.parentNode.parentNode.childNodes.querySelectorAll('li'));
			this.parentNode.parentNode.childNodes.classList.remove('is-active');
		} catch (error) {
			console.log(error);
		}
		this.parentNode.classList.add('is-active');
	}*/
});
// e: menu-ui 전체 MENU 클릭시 javascript

$(function () {
	setTimeout(function () {
		$('.lnb-menulist li').each(function () {
			if ($(this).children('ul').length) {
				$(this).prepend('<em></em>');
			}
		});
	}, 100);
	$('body').on('click', '.lnb-menulist-item > em', function (e) {
		$('.lnb-menulist-item').find('li').removeClass('is-active');
	});
	$('body').on('click', '.lnb-menulist em', function (e) {
		if ($(this).parent().hasClass('is-active')) {
			$(this).parent().removeClass('is-active');
		} else {
			$(this).parent().addClass('is-active').siblings().removeClass('is-active');
		}
	});

	// menu 이외 영역 클릭시 닫힘
	// $('body').on('click', function(e){
	// 	const $tgPoint = $(e.target);
	// 	const $callBtn = $tgPoint.hasClass('menu-open')
	// 	const $wrap = $tgPoint.parents('.menu').length < 1;	 
	// 	if ( $callBtn ) {
	// 		$('body').addClass('__menu-open');
	// 	} else if ( $wrap ) {
	// 		$('body').removeClass('__menu-open');
	// 	}
	// });
});



// S : datepicker 처리
/*
$(function () {
	// datepicker 적용
	if ($('.datepicker-wrap .datepicker').length > 0) {
		// 달력 : datepicker 기본 세팅
		$.datepicker.setDefaults({
			monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
			dateFormat: 'yy.mm.dd',
			showMonthAfterYear: true,
			yearSuffix: '년',
			buttonImageOnly: true,
			buttonImage: "/resources/images/ico-calendar.png",
			buttonText: "Select date"
		});
		$.each($('.datepicker-wrap .datepicker'), function () {
			var $this = $(this);
		
			if ($(this).data('year') == 'button') {
				$this.datepicker({
					beforeShow: function () {
						setTimeout(function () {
							drawChangeYear($this);
						}, 0);
					},
					onChangeMonthYear: function (y, m, ins) {
						setTimeout(function () {
							drawChangeYear($this);
						}, 0);
					},
				});
			} else {
				$this.datepicker();
			}
		});
	}
	// datepicker : 연도 버튼 컨트롤
	function drawChangeYear(datebox) {
		var prevStr = '<a class="ui-datepicker-prev ui-corner-all ws-year" id="wsPrevYear" style="left: 0px">이전 년</a>';
		var nextStr = '<a class="ui-datepicker-next ui-corner-all ws-year" id="wsNextYear" style="right: 0px;">다음 년</a>';
		$('.ui-datepicker-header').prepend(prevStr);
		$('.ui-datepicker-header').prepend(nextStr);

		$('#wsPrevYear').on('click', function (e) {
			e.preventDefault();
			$.datepicker._adjustDate($(datebox), -1, 'Y');
		});
		$('#wsNextYear').on('click', function (e) {
			e.preventDefault();
			$.datepicker._adjustDate($(datebox), 1, 'Y');
		});
	}
});
*/
// E : datepicker 처리
