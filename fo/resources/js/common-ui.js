'use strict';
// 인풋, 텍스트 글자수 카운트
let setTextCount = function (id, max) {
	let el = document.getElementById(id);
	el.addEventListener('keyup', function (e) {
		e.stopPropagation();
		if (Number(el.value.length) > max) {
			el.value = el.value.substr(0, max);
			//alert('글자 수가' + String(max) + '자를 초과했습니다.');
			alert('글자 수가' + max.toLocaleString() + '자를 초과했습니다.');
			//toLocaleString
		}
		//let txtCount = String(Number(el.value.length)) + '/' + String(max);
		let txtCount = Number(el.value.length).toLocaleString() + '/' + max.toLocaleString();
		el.nextElementSibling.querySelector('.count-txt').innerText = txtCount;
	});
};

let setSearchToggle = function setSearchToggle() {
	var icon = document.getElementById('searchIcon');
	var search = document.getElementById('search');
	var input = document.getElementById('algoliaSearch');

	if (!icon) {
		return;
	}

	icon.addEventListener('click', function (event) {
		search.classList.toggle('bd-is-visible');

		if (search.classList.contains('bd-is-visible')) {
			algoliaSearch.focus();
		}
	});

	window.addEventListener('keydown', function (event) {
		if (event.key === 'Escape') {
			return search.classList.remove('bd-is-visible');
		}
	});
};

// no image
function noImage(el) {
	el.setAttribute('src', '/resources/images/noimg.svg');
}

var resizeTimer = void 0;

// Dropdowns

document.addEventListener('DOMContentLoaded', function () {
	var $dropdowns = getAll('.dropdown:not(.is-hoverable)');
	const elDropItem = getAll('.dropdown-item:not([disabled])');
	const ctbox = document.querySelectorAll('.list-category');
	const ctTitle = document.querySelector('.category-box__title');
	const setDropdowns = () => {
		if ($dropdowns.length > 0) {
			$dropdowns.forEach($el => {
				$el.addEventListener('click', function (event) {
					event.stopPropagation();

					// let $dropdownHover = getAll('.dropdown:not(.is-hover)');
					// console.log($dropdownHover);
					// $dropdownHover.forEach(function ($el) {
					// 	$el.classList.remove('is-active');
					// });
					let $item = event.target.closest('.dropdown');
					let $itemActive = $item.classList.contains('is-active');
					if ($itemActive) {
						$item.classList.remove('is-active');
					} else {
						closeDropdowns();
						$item.classList.add('is-active');
					}

					// $el.classList.toggle('is-active');
				});
				// $el.addEventListener('mouseenter', function (event) {
				// 	event.stopPropagation();
				// 	$el.classList.toggle('is-hover');
				// });
				// $el.addEventListener('mouseleave', function (event) {
				// 	event.stopPropagation();
				// 	$el.classList.remove('is-hover');
				// });
			});
			document.addEventListener('click', function (event) {
				closeDropdowns();
			});
			elDropItem.forEach($el => {
				$el.addEventListener('click', function (event) {
					event.stopPropagation();
					getSiblings($el).forEach(function (subel, i) {
						subel.classList.remove('is-active');
					});
					$el.classList.add('is-active');

					//console.log(closest($el, 'dropdown').querySelector('.dropdown-trigger').querySelector('.button').querySelector('span'));
					//indexOf('Category') == -1
					if (closest($el, 'dropdown').querySelector('.dropdown-trigger').querySelector('.button').classList.contains('list')) {
						ctTitle.innerText = $el.innerText;
					} else if (closest($el, 'dropdown').querySelector('.dropdown-trigger').querySelector('.button').classList.contains('list-product')) {
						const $trigger = document.querySelector('[data-target="foru');
						closest($el, 'dropdown').querySelector('.dropdown-trigger').querySelector('.button').querySelector('span').innerText = $el.innerText;
						const item = getAll('.show-item');
						if ($trigger.innerText == closest($el, 'dropdown').querySelector('.dropdown-trigger').querySelector('.button').querySelector('span').innerText) {
							item.forEach(function (ele) {
								//ele.classList.add('is-active');
								if (ele.classList.contains('is-pagination')) {
									ele.classList.remove('is-block');
								} else if (ele.classList.contains('is-button')) {
									ele.classList.add('is-block');
								}
							});
						} else {
							item.forEach(function (ele) {
								//ele.classList.add('is-active');
								if (ele.classList.contains('is-button')) {
									ele.classList.remove('is-block');
								} else if (ele.classList.contains('is-pagination')) {
									ele.classList.add('is-block');
								}
							});
						}
					} else {
						closest($el, 'dropdown').querySelector('.dropdown-trigger').querySelector('.button').querySelector('span').innerText = $el.innerText;
					}
					//closest($el, 'dropdown').querySelector('.dropdown-trigger').querySelector('.button').querySelector('span').innerText = $el.innerText;
					// function dropdownMenu(ctbox) {

					// }
					//dropdownMenu();
					closeDropdowns();
				});
			});
		}
	};

	// breadcrumb 접근성관련 Tab 키 접근 이벤트 처리 focusin/focusout
	const breadcrumb = document.querySelector('.breadcrumb');
	if (breadcrumb) {
		let $dropdownHover = breadcrumb.querySelectorAll('.dropdown.is-hoverable');
		$dropdownHover.forEach($el => {
			$el.addEventListener('focusin', event => {
				// console.log(event.target);
				$dropdownHover.forEach($el => {
					$el.classList.remove('is-hover');
				});
				event.target.closest('.is-hoverable').classList.add('is-hover');
			});
			$el.addEventListener('focusout', event => {
				let currentTarget;
				let compareTarget = event.relatedTarget; // null 일 경우????
				if (compareTarget != '' && compareTarget != null) {
					// console.log(compareTarget);
					let compareTargetParentEl = compareTarget.parentElement.classList.contains('dropdown-trigger');
					if (compareTarget.classList == 'button' && compareTargetParentEl) {
						currentTarget = 'dropdown-item';
					} else {
						currentTarget = compareTarget.classList[0];
					}
				} else {
					currentTarget = '';
				}
				if (currentTarget != 'dropdown-item') {
					// console.log(event.target);
					event.target.closest('.is-hoverable').classList.remove('is-hover');
				}
			});
		});
	}

	setSearchToggle();
	setDropdowns();

	// scroll to top
	if (document.querySelector('.footer-top-area')) {
		let toTop = document.querySelector('.footer-top-area');

		toTop.addEventListener('click', function (e) {
			e.preventDefault();
			window.scroll({top: 0, left: 0, behavior: 'smooth'});
		});
	}

	// Cookies

	// var cookieBookModalName = 'bulma_closed_book_modal';
	//   var cookieBookModal = Cookies.getJSON(cookieBookModalName) || false;

	function closeDropdowns() {
		$dropdowns.forEach(function ($el) {
			$el.classList.remove('is-active');
		});
	}

	document.addEventListener('keydown', function (event) {
		var e = event || window.event;

		if (e.key === 'Escape') {
			let dropdownActive = document.querySelector('.dropdown.is-active');
			if (dropdownActive) {
				closeDropdowns();
			}
		}
	});

	// Spinner box
	var setUiSpinner = function uiSpinner() {
		const $spinner = document.querySelectorAll('[data-js-spinner]');

		Array.prototype.forEach.call($spinner, function ($element) {
			const $input = $element.querySelector('[data-js-spinner-input]');
			const $btnDecrement = $element.querySelector('[data-js-spinner-decrement]');
			const $btnIncrement = $element.querySelector('[data-js-spinner-increment]');
			const inputRules = {
				min: +$input.getAttribute('min'),
				max: +$input.getAttribute('max'),
				steps: +$input.getAttribute('steps') || 1,
				maxlength: +$input.getAttribute('maxlength') || null,
				minlength: +$input.getAttribute('minlength') || null,
			};
			let inputValue = +$input.value || 0;

			$input.addEventListener('input', handleInputUpdateValueInput, false);
			$element.addEventListener('keydown', handleMousedownDecrementSpinner, false);
			$btnDecrement.addEventListener('click', handleClickDecrementBtnDecrement, false);
			$btnIncrement.addEventListener('click', handleClickIncrementBtnIncrement, false);

			function handleInputUpdateValueInput() {
				let value = +$input.value;

				if (isNaN(value)) inputValue = 0;
				else inputValue = value;
			}

			function handleMousedownDecrementSpinner(event) {
				const keyCode = event.keyCode;
				const arrowUpKeyCode = 38;
				const arrowDownKeyCode = 40;

				if (keyCode === arrowDownKeyCode) handleClickDecrementBtnDecrement();
				else if (keyCode === arrowUpKeyCode) handleClickIncrementBtnIncrement();
			}

			function handleClickDecrementBtnDecrement() {
				if (!isGreaterThanMaxlength(inputValue - 1)) {
					if ($input.hasAttribute('min')) {
						if (inputValue > inputRules.min) decrement();
					} else decrement();
				}
			}

			function handleClickIncrementBtnIncrement() {
				if (!isGreaterThanMaxlength(inputValue + 1)) {
					if ($input.hasAttribute('max')) {
						if (inputValue < inputRules.max) increment();
					} else increment();
				}
			}

			function decrement() {
				inputValue -= inputRules.steps;
				if ($input.hasAttribute('max') && inputValue > $input.getAttribute('max')) {
					inputValue = +$input.getAttribute('max');
					$input.value = +inputValue.toFixed(12);
				} else $input.value = +inputValue.toFixed(12);
			}

			function increment() {
				inputValue += inputRules.steps;
				if ($input.hasAttribute('min') && inputValue < $input.getAttribute('min')) {
					inputValue = +$input.getAttribute('min');
					$input.value = +inputValue.toFixed(12);
				} else $input.value = +inputValue.toFixed(12);
			}

			function isGreaterThanMaxlength(value) {
				return value.toString().length > inputRules.maxlength && inputRules.maxlength !== null;
			}
		});
	};

	setUiSpinner();

	// Collapse / Accordion
	function initCollapse(elem, option) {
		document.addEventListener('click', function (e) {
			if (!e.target.matches(elem + ' [data-collapse]')) return;
			else {
				e.preventDefault();
				//let parentEl = e.target.parentElement.parentElement;
				let parentEl = e.target.parentElement.closest('.collapse-item');
				if (parentEl.classList.contains('is-active')) {
					parentEl.classList.remove('is-active');
				} else {
					// accordion
					if (option == true) {
						let accId = parentEl.getAttribute('data-accordion');
						let accEl = document.getElementById(accId);
						let elementList = accEl.querySelectorAll('.collapse-item');
						Array.prototype.forEach.call(elementList, function (e) {
							e.classList.remove('is-active');
						});
					}
					parentEl.classList.add('is-active');
				}
			}
		});
	}
	initCollapse('.collapse.is-accordion', true);
	initCollapse('.collapse.is-collapse', false);

	// search modal popup action
	let elHKeyword = document.querySelectorAll('.keyword-header-search');
	if (elHKeyword) {
		elHKeyword.forEach(function (el, key) {
			el.addEventListener('click', function (event) {
				event.stopPropagation();
				document.querySelector('.header-search-input').value = this.innerText;
			});
		});
	}
	let elHKwDelete = document.querySelectorAll('.button-delete-hkeyword');
	if (elHKwDelete) {
		elHKwDelete.forEach(function (el, key) {
			el.addEventListener('click', function (event) {
				event.stopPropagation();
				closest(this, 'header-search-layer__recent-item').remove();
			});
		});
	}
	// let elHKwDelete = document.querySelectorAll('.button-delete-hkeyword');
	let elHKwDelAll = document.querySelector('.button-delete-hkeyword__all');
	if (elHKwDelAll) {
		elHKwDelAll.addEventListener('click', function (event) {
			event.stopPropagation();
			let elHKwItem = document.querySelectorAll('.header-search-layer__recent-item');
			if (elHKwItem) {
				elHKwItem.forEach(function (el, key) {
					el.remove();
				});
			}
		});
	}

	// // show/hide control
	// var $showButtons = getAll('.show-button');
	// var $hideCloses = getAll('.hide-this-button');
	// //var focusTarget = [];
	// if ($showButtons.length > 0) {
	// 	$showButtons.forEach(function ($el) {
	// 		$el.addEventListener('click', function () {
	// 			let target = $el.dataset.target;
	// 			let focus = $el.dataset.focus;
	// 			//console.log(target, focus);
	// 			//document.getElementById(target).classList.toggle('is-active');
	// 			document.getElementById(target).classList.add('is-active');
	// 			if (!focus) return;
	// 			else {
	// 				document.getElementById(focus).focus();
	// 			}
	// 		});
	// 	});
	// }
	// if ($hideCloses.length > 0) {
	// 	$hideCloses.forEach(function ($el) {
	// 		$el.addEventListener('click', event => {
	// 			//let target = event.target
	// 			let ele = event.target.closest('.is-active');
	// 			let textarea = $el.dataset.clear;
	// 			let clear = $el.dataset.hide;
	// 			let hide = document.querySelectorAll('[data-target="' + clear + '"]')[0];
	// 			document.getElementById(textarea).value = null;
	// 			//console.log(hide);
	// 			hide.focus();
	// 			ele.classList.remove('is-active');
	// 		});
	// 	});
	// }

	// password container
	let elPassword = document.querySelectorAll('.password-container');
	if (elPassword) {
		elPassword.forEach(function (el, key) {
			el.querySelector('.password-container-button').addEventListener('click', function (event) {
				event.stopPropagation();
				this.classList.toggle('is-active');
				let passwordInput = el.querySelector('.input');
				if (this.classList.contains('is-active')) {
					passwordInput.type = 'text';
				} else {
					passwordInput.type = 'password';
				}
			});
		});
	}

	//관심등록버튼
	/*ttonToggle(e) {
		if (e.classList.contains('--on')) {
			e.classList.remove('--on');
		} else {
			e.classList.add('--on');
		}
	}*/

	// Header scroll check
	if (document.querySelector('#header')) {
		let headerH = document.querySelector('#header').clientHeight;
		const funcScrollCheck = () => {
			let scroll = window.scrollY;
			const elBody = document.querySelector('html');
			if (scroll > 22) {
				elBody.classList.add('__header-sticky');
			} else {
				elBody.classList.remove('__header-sticky');
			}
		};
		funcScrollCheck();

		window.addEventListener('scroll', function () {
			funcScrollCheck();
		});
	}
	if (document.querySelector('#top')) {
		let headerH = document.querySelector('#header').clientHeight;
		let footerH = document.querySelector('#footer').clientHeight;
		const funcScrollCheck = () => {
			let scroll = window.scrollY;
			const elBody = document.querySelector('html');
			// const elTop = document.getElementById('top');
			const elTop = document.querySelector('.footer .footer-top');
			const footer = document.body.offsetHeight - window.innerHeight - footerH + elTop.clientHeight;
			if (scroll > 22) {
				//시작
				elTop.classList.add('__foot-sticky');
				if (scroll > footer) {
					elTop.classList.add('__bottom-sticky');
				} else {
					elTop.classList.remove('__bottom-sticky');
				}
			} else {
				elTop.classList.remove('__foot-sticky');
				elTop.classList.remove('__bottom-sticky');
			}
			//console.log(scroll, footer);
		};
		funcScrollCheck();
		window.addEventListener('scroll', function () {
			funcScrollCheck();
		});
	}
});

// 웹접근성 Dynamic Tab
function TabsAutomatic(groupNode) {
	this.tablistNode = groupNode;

	this.tabs = [];

	this.firstTab = null;
	this.lastTab = null;

	// ie11
	let tabs = [];
	new Set(this.tablistNode.querySelectorAll('[role=tab]')).forEach(function (v) {
		tabs.push(v);
	});
	this.tabs = tabs;
	// this.tabs = Array.from(this.tablistNode.querySelectorAll('[role=tab]'));

	this.tabpanels = [];

	for (var i = 0; i < this.tabs.length; i += 1) {
		var tab = this.tabs[i];
		var tabpanel = document.getElementById(tab.getAttribute('aria-controls'));

		tab.tabIndex = -1;
		tab.setAttribute('aria-selected', 'false');
		this.tabpanels.push(tabpanel);

		tab.addEventListener('keydown', this.onKeydown.bind(this));
		tab.addEventListener('click', this.onClick.bind(this));

		if (!this.firstTab) {
			this.firstTab = tab;
		}
		this.lastTab = tab;
	}
	this.setSelectedTab(this.firstTab, false);
}
TabsAutomatic.prototype.setSelectedTab = function (currentTab, setFocus) {
	if (typeof setFocus !== 'boolean') {
		setFocus = true;
	}
	for (var i = 0; i < this.tabs.length; i += 1) {
		var tab = this.tabs[i];
		if (currentTab === tab) {
			tab.setAttribute('aria-selected', 'true');
			tab.parentElement.classList.add('is-active');
			tab.removeAttribute('tabindex');
			this.tabpanels[i].setAttribute('aria-hidden', 'false');
			this.tabpanels[i].classList.add('is-active');
			if (setFocus) {
				tab.focus();
			}
		} else {
			tab.parentElement.classList.remove('is-active');
			tab.setAttribute('aria-selected', 'false');
			tab.tabIndex = -1;
			this.tabpanels[i].setAttribute('aria-hidden', 'true');
			this.tabpanels[i].classList.remove('is-active');
		}
	}
};
TabsAutomatic.prototype.setSelectedToPreviousTab = function (currentTab) {
	var index;

	if (currentTab === this.firstTab) {
		this.setSelectedTab(this.lastTab);
	} else {
		index = this.tabs.indexOf(currentTab);
		this.setSelectedTab(this.tabs[index - 1]);
	}
};
TabsAutomatic.prototype.setSelectedToNextTab = function (currentTab) {
	var index;

	if (currentTab === this.lastTab) {
		this.setSelectedTab(this.firstTab);
	} else {
		index = this.tabs.indexOf(currentTab);
		this.setSelectedTab(this.tabs[index + 1]);
	}
};
/* EVENT HANDLERS */
TabsAutomatic.prototype.onKeydown = function (event) {
	var tgt = event.currentTarget,
		flag = false;

	switch (event.key) {
		case 'ArrowLeft':
			this.setSelectedToPreviousTab(tgt);
			flag = true;
			break;

		case 'ArrowRight':
			this.setSelectedToNextTab(tgt);
			flag = true;
			break;

		case 'Home':
			this.setSelectedTab(this.firstTab);
			flag = true;
			break;

		case 'End':
			this.setSelectedTab(this.lastTab);
			flag = true;
			break;

		default:
			break;
	}

	if (flag) {
		event.stopPropagation();
		event.preventDefault();
	}
};
TabsAutomatic.prototype.onClick = function (event) {
	this.setSelectedTab(event.currentTarget);
};
// Initialize tablist
window.addEventListener('load', function () {
	var tablists = document.querySelectorAll('[role=tablist]');
	for (var i = 0; i < tablists.length; i++) {
		// TabsAutomatic(tablists[i]);
		new TabsAutomatic(tablists[i]);
	}
});

// Utils
function getAll(selector) {
	var parent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

	return Array.prototype.slice.call(parent.querySelectorAll(selector), 0);
}

// var world = document.getElementById('world'); var closest = closest(world, 'item');
// function closest(el, classname) {
// 	if (el.parentNode) {
// 		if (el.parentNode.className.includes(classname)) {
// 			return el.parentNode;
// 		} else {
// 			return closest(el.parentNode, classname);
// 		}
// 	} else {
// 		return false;
// 	}
// }

/*
s : mp menu : Products Categories
*/
// get all siblings
function getSiblings(e) {
	// for collecting siblings
	let siblings = [];
	// if no parent, return no sibling
	if (!e.parentNode) {
		return siblings;
	}
	// first child of the parent node
	let sibling = e.parentNode.firstChild;

	// collecting siblings
	while (sibling) {
		if (sibling.nodeType === 1 && sibling !== e) {
			siblings.push(sibling);
		}
		sibling = sibling.nextSibling;
	}
	return siblings;
}

function extend(a, b) {
	for (var key in b) {
		if (b.hasOwnProperty(key)) {
			a[key] = b[key];
		}
	}
	return a;
}
function hasParent(e, id) {
	if (!e) return false;
	var el = e.target || e.srcElement || e || false;
	while (el && el.id != id) {
		el = el.parentNode || false;
	}
	return el !== false;
}
// returns the depth of the element "e" relative to element with id=id
// for this calculation only parents with classname = waypoint are considered
function getLevelDepth(e, id, waypoint, cnt) {
	cnt = cnt || 0;
	if (e.id.indexOf(id) >= 0) return cnt;

	if (e.classList.contains(waypoint)) {
		++cnt;
	}
	return e.parentNode && getLevelDepth(e.parentNode, id, waypoint, cnt);
}

// http://coveroverflow.com/a/11381730/989439
function mobilecheck() {
	var check = false;
	(function (a) {
		if (
			/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
				a,
			) ||
			/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
				a.substr(0, 4),
			)
		)
			check = true;
	})(navigator.userAgent || navigator.vendor || window.opera);
	return check;
}

// returns the closest element to 'e' that has class "classname"
function closest(e, classname) {
	if (e.classList.contains(classname)) {
		return e;
	}
	return e.parentNode && closest(e.parentNode, classname);
}

/*
	Window resize handler
	*/
// window resize
function windowResize() {
	window.clearTimeout(resizeTimer);

	// 1038px -> 1054px
	// let windowWidth = window.outerWidth;
	// if (windowWidth > 1054) {
	// 	console.log(windowWidth);
	// }

	// resizeTimer = window.setTimeout(function () {
	// 	setNavbarVisibility();
	// }, 10);
}

// add to global namespace
document.addEventListener('DOMContentLoaded', function () {
	// s: allmenu 카테고리 메뉴
	function openAllmenu(el) {}
	function closeAllmenu(el) {}
	if (document.querySelector('.allmenu-button')) {
		const elOpenAllBtn = document.querySelector('.allmenu-button');
		elOpenAllBtn.addEventListener('click', function (event) {
			event.stopPropagation();
			event.preventDefault();
			if (elOpenAllBtn.classList.contains('is-active')) {
				document.querySelector('html').classList.remove('__allmenu-pc');
				elOpenAllBtn.classList.remove('is-active');
				// has-child 의 모든 is-active 제거
				const elMpChildLi = document.querySelectorAll('.mp-level .has-child');
				elMpChildLi.forEach(target => {
					target.classList.remove('is-active');
				});
			} else {
				document.querySelector('html').classList.add('__allmenu-pc');
				elOpenAllBtn.classList.add('is-active');
				// 첫번째 li 요소에 is-hover + mp-level-open
				let firstLi = document.querySelector('.allmenu-list').querySelector('li');
				firstLi.classList.add('is-active');
			}
		});
	}
	// 모바일 aside 카테고리 버튼
	if (document.querySelector('.aside-category-button')) {
		const elOpenAllBtn = document.querySelectorAll('.aside-category-button');

		const elAllList = document.querySelector('.allmenu-list');
		let elAllListElem = [];
		for (var i = 0; i < elAllList.childNodes.length; i++) {
			if (elAllList.childNodes[i].nodeName === 'LI') {
				elAllListElem.push(elAllList.childNodes[i]);
			}
		}

		elOpenAllBtn.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				document.querySelector('html').classList.add('__allmenu-open');

				getSiblings(target).forEach(function (subel, i) {
					subel.classList.remove('is-active');
				});
				// 카테고리 목록의 첫번째 자식 요소들 li 하나에만 is-active 부여
				for (var i = 0; i < elAllListElem.length; i++) {
					elAllListElem[i].classList.remove('is-active');
				}
				elAllListElem[target.dataset.target - 1].classList.add('is-active');
				target.classList.add('is-active');
			});
		});
	}
	// 초기화
	function getLevelDepth(e, id, waypoint, cnt) {
		cnt = cnt || 0;
		if (e.id.indexOf(id) >= 0) return cnt;

		if (e.classList.contains(waypoint)) {
			++cnt;
		}
		return e.parentNode && getLevelDepth(e.parentNode, id, waypoint, cnt);
	}
	if (document.querySelector('.allmenu-list')) {
		// data-level 부여
		const levels = Array.prototype.slice.call(document.querySelectorAll('.mp-level'));
		levels.forEach(function (el, i) {
			el.setAttribute('data-level', getLevelDepth(el, 'mp-menu', 'mp-level'));
		});

		// li 하위 mp-level ul 에 node 가 있는지 체크후 has-child 클래스 부여
		const elMpLi = document.querySelectorAll('.mp-level');
		elMpLi.forEach(target => {
			if (target.querySelector('li')) {
				target.parentNode.classList.add('has-child');
			}
		});

		const elHasChildA = document.querySelectorAll('.has-child > a');
		elHasChildA.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				event.preventDefault();
				getSiblings(target.parentNode).forEach(function (subel, i) {
					subel.classList.remove('is-active');
				});
				target.parentNode.classList.toggle('is-active');
			});
			// PC mouseover
			target.addEventListener('mouseover', function (event) {
				event.stopPropagation();
				event.preventDefault();
				const elAllLevel = document.querySelectorAll('.mp-level');
				getSiblings(target.parentNode).forEach(function (subel, i) {
					subel.classList.remove('is-active');
				});
				target.parentNode.classList.add('is-active');
			});
		});
	}
	if (document.querySelector('.allmenu-back-button')) {
		const elOpenAllBtn = document.querySelector('.allmenu-back-button');
		elOpenAllBtn.addEventListener('click', function (event) {
			event.stopPropagation();
			document.querySelector('html').classList.remove('__allmenu-open');
			elOpenAllBtn.classList.remove('is-active');
			// 모바일 창 닫을 시 has-child 의 모든 is-active 제거
			const elMpChildLi = document.querySelectorAll('.mp-level .has-child');
			elMpChildLi.forEach(target => {
				target.classList.remove('is-active');
			});
		});
	}
	// e: allmenu 카테고리 메뉴
	// Events
	// windowResize();
	// window.addEventListener('resize', windowResize);
});

document.addEventListener('DOMContentLoaded', function () {
	// 검색 값 초기화
	if (document.querySelector('.mypg-clear-btn')) {
		let clearBtn = document.querySelector('.mypg-clear-btn');
		let searchText = document.querySelectorAll('.mypg-search-input');

		clearBtn.addEventListener('click', () => {
			searchText.forEach(target => {
				target.value = '';
			});
		});
	}

	// 체크박스 전체 선택 및 해제
	if (document.querySelector('.all-checkbox')) {
		/* let selCheck = document.querySelectorAll('.all-checkbox-item');
		let allCheck = document.querySelector('.all-checkbox');

		// all check box check event
		allCheck.addEventListener('click', event => {
			if (allCheck.checked) {
				selCheck.forEach(target => {
					target.checked = true;
				});
			} else {
				selCheck.forEach(target => {
					target.checked = false;
				});
			}
		}); */
		//2022-11-15 전체 체크 : 해제 js 수정
		let $chk = false;
		let $checked = getAll('.all-checkbox');
		if ($checked.length > 0) {
			$checked.forEach(function ($el) {
				$el.addEventListener('click', function () {
					var chk = $el.checked;
					var parent = $el.closest('.table');
					var target = parent.querySelectorAll('.all-checkbox-item');
					//console.log(chk);
					checkAll(target, chk);
				});
			});
		}
		function checkAll(target, chk) {
			$chk = $chk ? false : true;
			for (var i = 0; i < target.length; i++) {
				target[i].checked = chk;
				//console.log($chk);
			}
		}
	}

	// share click event
	if (document.querySelectorAll('.share-btn')) {
		let shareBtn = document.querySelectorAll('.share-btn');
		shareBtn.forEach(target => {
			target.addEventListener('click', () => {
				target.classList.toggle('is-active');
			});
		});
	}
	// aside title collapse event
	if (document.querySelectorAll('.asection-title-button')) {
		let aTitleBtn = document.querySelectorAll('.asection-title-button');
		aTitleBtn.forEach(target => {
			target.addEventListener('click', () => {
				if (target.classList.contains('is-active')) {
					target.classList.remove('is-active');
					target.closest('.asection').querySelector('.asection-content').classList.remove('is-active');
				} else {
					target.classList.add('is-active');
					target.closest('.asection').querySelector('.asection-content').classList.add('is-active');
				}
			});
		});
	}

	// header menu click event
	if (document.querySelector('.header-menu')) {
		let headerBtn = document.querySelector('.header-menu');
		headerBtn.addEventListener('click', () => {
			document.querySelector('html').classList.toggle('__aside-close');
		});
	}

	// mobile menu open click event
	if (document.querySelector('.header-mobile-menu')) {
		let mobileBtn = document.querySelector('.header-mobile-menu');
		mobileBtn.addEventListener('click', () => {
			document.querySelector('html').classList.add('__mobile-menu');
		});
	}
	// mobile menu close click event
	if (document.querySelector('.header-mobile-close')) {
		let asideMoreBtn = document.querySelectorAll('.header-mobile-close');
		asideMoreBtn.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				// 모바일 창 닫을 시 has-child 의 모든 is-active 제거
				const elMpChildLi = document.querySelectorAll('.mp-level .has-child');
				elMpChildLi.forEach(target => {
					target.classList.remove('is-active');
				});
				document.querySelector('html').classList.remove('__mobile-menu', '__allmenu-open');
			});
		});
	}

	// aside-more-button click event
	if (document.querySelectorAll('.aside-more-button')) {
		let asideMoreBtn = document.querySelectorAll('.aside-more-button');
		asideMoreBtn.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();

				if (target.classList.contains('is-active')) {
					target.classList.remove('is-active');
					target.querySelector('span').innerText = 'Show more';
					target.closest('.asection').classList.remove('is-active');
				} else {
					target.classList.add('is-active');
					target.querySelector('span').innerText = 'Show less';
					target.closest('.asection').classList.add('is-active');
				}
			});
		});
	}

	// join textarea count
	if (document.getElementById('joinIntroduction') || document.getElementById('myFormIntroduction')) {
		if (document.getElementById('joinIntroduction')) {
			setTextCount('joinIntroduction', 2000);
		} else if (document.getElementById('myFormIntroduction')) {
			setTextCount('myFormIntroduction', 2000);
		}
	}

	// product detail textarea count
	if (document.getElementById('inqTextarea')) {
		setTextCount('inqTextarea', 10000);
	}

	// all checkbox ment
	if (document.querySelector('.mypage-del')) {
		document.querySelector('.mypage-del').addEventListener('click', () => {
			let qaCheck = document.querySelectorAll('.all-checkbox-item');
			let qaMent = false;

			qaCheck.forEach(target => {
				if (target.checked == true) {
					qaMent = true;
				}
			});

			if (qaMent == false) {
				alert('선택한 항목이 없습니다.');
			} else {
				const delMent = confirm('선택한 항목이 모두 삭제됩니다. 삭제하시겠습니까 ?');

				if (delMent == true) {
					alert('삭제되었습니다.');
				} else {
					alert('취소되었습니다.');
				}
			}
		});
	}

	if (document.querySelectorAll('.prod-tag-btn')) {
		document.querySelectorAll('.prod-tag-btn').forEach(target => {
			target.addEventListener('click', () => {
				document.querySelectorAll('.prod-tag-btn').forEach(event => {
					event.classList.remove('on-active');
				});
				target.classList.add('on-active');
			});
		});
	}

	if (document.querySelector('.basic-tabs-ul')) {
		document.querySelectorAll('.basic-tabs-ul li').forEach(target => {
			target.addEventListener('click', () => {
				document.querySelectorAll('.basic-tabs-ul li').forEach(event => {
					event.classList.remove('is-active');
				});
				target.classList.add('is-active');
			});
		});
	}

	if (document.querySelectorAll('.ctg-ver-item')) {
		let ctgVerItem = document.querySelectorAll('.ctg-ver-item');

		ctgVerItem.forEach(function (item) {
			item.addEventListener('click', function (e) {
				ctgVerItem.forEach(function (ctg) {
					ctg.classList.remove('is-active');
				});
				item.classList.add('is-active');
			});
		});
	}

	if (document.querySelectorAll('.category-mobile-button')) {
		let mobileCtgBtn = document.querySelectorAll('.category-mobile-button');
		let mobileTagBox = document.querySelector('.category-mobile');

		mobileCtgBtn.forEach(function (btn) {
			btn.addEventListener('click', function (e) {
				if (btn.parentElement.classList.contains('is-open')) {
					// btn.classList.add('is-active');
					btn.parentElement.classList.remove('is-open');
					// btn.parentElement.style.maxHeight = 600 + 'px';
				} else {
					// btn.classList.remove('is-active');
					btn.parentElement.classList.add('is-open');
					// btn.parentElement.style.maxHeight = tagBox + 'px';
				}
			});
		});
	}

	// 카탈로그 선택/다운로드 작동
	if (document.querySelector('.button-select-catalog') && document.querySelector('.list__prod')) {
		let elSelCatl = document.querySelector('.button-select-catalog');
		const checkAll = mode => {
			const elChkAll = document.querySelector('.list__prod').querySelectorAll('.checkbox-area');
			elChkAll.forEach(target => {
				if (mode === 'add') {
					target.classList.add('is-active');
					// } else {
					// 	target.classList.remove('is-active');
				}
			});
		};

		elSelCatl.addEventListener('click', function (e) {
			e.stopPropagation();
			if (!elSelCatl.classList.contains('is-active')) {
				elSelCatl.classList.add('is-active');
				elSelCatl.innerText = 'Download Catalog';
				checkAll('add');
			}
			// if (elSelCatl.classList.contains('is-active')) {
			// 	elSelCatl.classList.remove('is-active');
			// 	elSelCatl.innerText = 'Select Catalog';
			// 	checkAll('remove');
			// 	console.log('test');
			// } else {
			// 	elSelCatl.classList.add('is-active');
			// 	elSelCatl.innerText = 'Download Catalog';
			// 	checkAll('add');
			// }
		});
	}

	// 행사전시 추가
	if (document.querySelector('.download-button') && document.querySelector('.ex-list-card')) {
		let elSelCat2 = document.querySelector('.download-button');
		const checkAll = mode => {
			const elChkAl2 = document.querySelector('.ex-list-card').querySelectorAll('.echeck');
			elChkAl2.forEach(target => {
				if (mode === 'add') {
					target.classList.add('is-active');
				} else {
					target.classList.remove('is-active');
				}
			});
		};

		elSelCat2.addEventListener('click', function (e) {
			e.stopPropagation();
			if (elSelCat2.classList.contains('is-active')) {
				elSelCat2.classList.remove('is-active');
				elSelCat2.innerText = 'Select Catalog';
				checkAll('remove');
			} else {
				elSelCat2.classList.add('is-active');
				elSelCat2.innerText = 'Download Catalog';
				checkAll('add');
			}
		});
	}

	// 전시상담홈 공유버튼
	if (document.querySelectorAll('.button-share-open')) {
		let elVar = document.querySelectorAll('.button-share-open');
		elVar.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				target.closest('.list-item').classList.toggle('is-hover');
			});
		});
	}

	if (document.querySelectorAll('.list-button')) {
		const listBtn = document.querySelectorAll('.list-button');
		const thumbBtn = document.querySelector('.thumbnail-button');
		const listHtml = document.querySelector('.ex-list-card');

		listBtn.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				target.classList.add('is-active');
				thumbBtn.classList.remove('is-active');
				listHtml.classList.add('list2');
			});
		});
	}
	if (document.querySelectorAll('.thumbnail-button')) {
		const thumbBtn = document.querySelectorAll('.thumbnail-button');
		const listBtn = document.querySelector('.list-button');
		const listHtml = document.querySelector('.ex-list-card');

		thumbBtn.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				target.classList.add('is-active');
				listBtn.classList.remove('is-active');
				listHtml.classList.remove('list2');
			});
		});
	}

	// 카테고리 필터 리셋 버튼 동장
	if (document.querySelectorAll('.filter-reset')) {
		const elFilterRst = document.querySelectorAll('.filter-reset');
		elFilterRst.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				target.classList.add('is-loading');
				setTimeout(() => {
					target.classList.remove('is-loading');
				}, 1000);
			});
		});
	}

	// 카테고리 필터 open/close
	if (document.querySelectorAll('.filter-mobile-button')) {
		const elFilterMBtn = document.querySelector('.filter-mobile-button');
		const elFilterMBtns = document.querySelectorAll('.filter-mobile-button');
		const elFilterClose = document.querySelectorAll('.filter-close');
		const elFilterCancel = document.querySelectorAll('.filter-cancel');
		const elListNav = document.querySelector('.comp-list-nav');
		const elHtml = document.querySelector('html');
		const aside = document.querySelector('.aside');

		elFilterMBtns.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				target.classList.toggle('is-active');
				elListNav.classList.toggle('is-active');
				elHtml.classList.toggle('__filter-active');
				aside.classList.toggle('__filter-active');
				elListNav.setAttribute('tabindex', 0);
				elListNav.focus();
				//console.log(document.activeElement);
			});
		});
		elFilterClose.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				elListNav.classList.toggle('is-active');
				elFilterMBtn.classList.remove('is-active');
				elHtml.classList.toggle('__filter-active');
				aside.classList.toggle('__filter-active');
			});
		});
		elFilterCancel.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				elListNav.classList.toggle('is-active');
				elFilterMBtn.classList.remove('is-active');
				elHtml.classList.toggle('__filter-active');
				aside.classList.toggle('__filter-active');
			});
		});
	}

	// 인콰이어리 슬라이드 inquire slide
	if (document.querySelector('.inquire-wrap.is-slide')) {
		let inqSlideBtn = document.querySelector('.button-inquire-more');
		let inqSlideBtnClose = document.querySelector('.button-inquire-cancel');
		let inqSlide = document.querySelector('.inquire-wrap.is-slide');
		inqSlideBtn.addEventListener('click', function () {
			inqSlide.classList.add('is-active');
		});
		inqSlideBtnClose.addEventListener('click', function () {
			inqSlide.classList.remove('is-active');
		});
	}

	// 상품상세 anchor tabs scroll
	if (document.querySelector('.tabs.is-anchor')) {
		let anchorScroll = document.querySelectorAll('a[role=anchorlink]');
		anchorScroll.forEach(elem => {
			elem.addEventListener('click', e => {
				e.preventDefault();
				let activeTab = e.target;
				for (; activeTab.nodeName != 'LI'; activeTab = activeTab.parentElement);

				//	let allTab = activeTab.parentElement.querySelectorAll('li');
				//	allTab.forEach(event => {
				//		event.classList.remove('is-active');
				//	});
				//	activeTab.classList.add('is-active');

				let block = document.querySelector(elem.getAttribute('href')),
					offset = elem.dataset.offset ? parseInt(elem.dataset.offset) : 0,
					bodyOffset = document.body.getBoundingClientRect().top;
				window.scrollTo({
					top: block.getBoundingClientRect().top - bodyOffset + offset - 100,
					behavior: 'smooth',
				});
			});
		});

		// 메뉴 링크 대비 element id값
		let scrollIds = [];
		anchorScroll.forEach(function (item, index) {
			scrollIds[index] = item.getAttribute('href');
		});

		window.addEventListener('scroll', function (e) {
			// console.log(this.document.querySelector('.detail-tabs').offsetTop);
			// console.log(this.document.querySelector('.detail-tabs').scrollHeight);
			let scrollTop = this.scrollY + 100;
			// current scroll item - id
			let currentArry = scrollIds.map(function (item) {
				let blockOffsetTop = document.querySelector(item).offsetTop;
				if (blockOffsetTop < scrollTop) {
					return item;
				}
			});
			let current = currentArry.filter(function (item) {
				return item != null;
			});
			// console.log(current);
			// 스크롤시 현재 element 확인후 anchor tab > li 클래스 추가
			if (current.length) {
				let currentId = current[current.length - 1];
				let currentTab = document.querySelector("a[href='" + currentId + "']").parentElement;
				let allTab = currentTab.parentElement.querySelectorAll('li');
				allTab.forEach(item => {
					let tabHref = item.children[0].getAttribute('href');
					if (tabHref === currentId) {
						item.classList.add('is-active');
					} else {
						item.classList.remove('is-active');
					}
				});
			}
		});
	}

	//모바일시 가로메뉴 해당 위치 스크롤
	if (document.querySelector('.nav-mobile')) {
		let navActive = document.querySelector('.nav-mobile .is-active');
		navActive.scrollIntoView({behavior: 'smooth', block: 'nearest', inline: 'center'});
	}

	// filter more less js : 05.offer/offer_list, 01.products/products_list
	const $moreButton = document.querySelectorAll('.button-see-more');
	let isHide = true;
	$moreButton.forEach(item => {
		item.addEventListener('click', event => {
			const currentBtn = event.target;
			const items = currentBtn.previousElementSibling.querySelectorAll('.filter-checkbox');
			currentBtn.textContent = isHide ? 'See Less' : 'See More';
			isHide = !isHide;
			// console.log(items);
			items.forEach(item => item.classList.toggle('hide'));
		});
	});

	//inquire anchor
	if(document.querySelector('.inquire-button')) {
		let buttonInquire = document.querySelectorAll('.inquire-button');
		const areaInquire = document.querySelector('.section-inquire');
		
		buttonInquire.forEach(item => {
			item.addEventListener('click', () => {
				areaInquire.scrollIntoView({behavior: 'smooth'});
				areaInquire.querySelector('form').setAttribute('tabindex', 0);
				areaInquire.querySelector('form').focus();
				//console.log(document.activeElement);
			})
		})

		let footerH = document.querySelector('#footer').clientHeight;
		const funcScrollCheck = () => {
			let scroll = window.scrollY;
			// const elTop = document.getElementById('top');
			const targetEl = document.querySelector('.product-layout-main .inquire-button');

			let el = targetEl.getBoundingClientRect().top;
			//console.log(el, scroll);
			if (scroll > 315) {
				targetEl.classList.add('__inquire-sticky');
			} else {
				targetEl.classList.remove('__inquire-sticky');
			}
			//console.log(scroll, footer);
		};
		funcScrollCheck();
		window.addEventListener('scroll', function () {
			funcScrollCheck();
		});
		
	}
	//console.log(document.activeElement);

	//상품 상세 anchor sticky
	// if(document.querySelector('.is-anchor')) {
	// 	let sticky = document.querySelector('.is-anchor');
	// 	let h = document.getElementById("header").clientHeight;
	// 	let c = document.querySelector('.product-particular').clientWidth;
		
	// 	let stuck = false;
	// 	let stickPoint = getDistance();

	// 	function getDistance() {
	// 	let topDist = sticky.offsetTop;
	// 		return topDist;
	// 	}		
	// 	window.onscroll = function(e) {
	// 		const distance = getDistance() - window.pageYOffset;
	// 		const offset = window.pageYOffset;
	// 		if ( (distance <= 0) && !stuck) {
	// 			sticky.classList.add('sticky-fixed');
	// 			sticky.style.position = 'fixed';
	// 			sticky.style.top = h + 'px';
	// 			stuck = true;
	// 		} else if (stuck && (offset <= stickPoint)){
	// 			sticky.classList.remove('sticky-fixed');
	// 			sticky.style.position = 'static';
	// 			stuck = false;
	// 		}
	// 	}
	// }
});

// Toast message
function displayToast(position, type, msg) {
	bulmaToast.toast({
		message: msg,
		type: type,
		position: position.toLowerCase().replace(' ', '-'),
		dismissible: true,
		duration: 2000,
		pauseOnHover: true,
		animate: {in: 'fadeIn', out: 'fadeOut'},
	});
}

//drag and drop
function dragAndDrop() {
	const $ = select => document.querySelectorAll(select);
	const draggables = $('.draggable-box');
	const containers = $('.draggable-container');
	//let eventtypeStart = mobilecheck() ? 'touchmove' : 'dragstart';
	let eventtypeStart = mobilecheck() ? 'touchstart' : 'dragstart';
	let eventtype = mobilecheck() ? 'touchmove' : 'dragover'; //dragover
	//let eventtypeEnd = mobilecheck() ? 'touchend' : 'dragend';
	let eventtypeEnd = mobilecheck() ? 'touchend' : 'dragend';

	draggables.forEach(el => {
		el.addEventListener(eventtypeStart, () => {
			el.classList.add('dragging');
		});
		el.addEventListener(eventtypeEnd, () => {
			el.classList.remove('dragging');
		});
	});
	containers.forEach(container => {
		container.addEventListener(eventtype, e => {
			e.preventDefault();
			const afterElement = getDragAfterElement(container, e.clientY); //(결국 드래그하여 마지막 영역의 엘리먼트를 반환합니다.)
			const currentDraggable = document.querySelector('.dragging'); //현재 내가 잡은 엘리먼트
			container.insertBefore(currentDraggable, afterElement); //마지막까지 드래그한 엘리먼트 앞에 현재 내가 잡은 엘리먼트를 삽입 합니다.
		});
	});
}
function getDragAfterElement(container, y) {
	const draggableElements = [...container.querySelectorAll('.draggable-box:not(.dragging)')];
	return draggableElements.reduce(
		(closest, child) => {
			const box = child.getBoundingClientRect(); //해당 엘리먼트에 top값, height값 담겨져 있는 메소드를 호출해 box변수에 할당
			const offset = y - box.top - box.height / 2; //수직 좌표 - top값 - height값 / 2의 연산을 통해서 offset변수에 할당
			if (offset < 0 && offset > closest.offset) {
				// (예외 처리) 0 이하 와, 음의 무한대 사이에 조건
				return {offset: offset, element: child}; // Element를 리턴
			} else {
				return closest;
			}
		},
		{offset: Number.NEGATIVE_INFINITY},
	).element;
}

// content-breadcrumb 초기화
window.addEventListener('load', function () {
	if (document.querySelector('.content-breadcrumb') && document.querySelector('.header-breadcrumb')) {
		document.querySelector('.header-breadcrumb').innerHTML = document.querySelector('.content-breadcrumb').innerHTML;
	}
});

document.addEventListener('DOMContentLoaded', function () {
	// new product 2뎁스 category on/off
	const productNewCategory = document.querySelector('.prod-cate');
	if (productNewCategory) {
		const btnView = productNewCategory.querySelector('.button-view');
		btnView.addEventListener('click', function (e) {
			const activeCategory = e.target.closest('.prod-tag');
			if (activeCategory.classList.contains('is-active')) {
				activeCategory.classList.remove('is-active');
			} else {
				activeCategory.classList.add('is-active');
			}
		});
	}

	// my page LNB on/off menu
	const mypageLnb = document.querySelector('.my-lnb');
	if (mypageLnb) {
		const menuItems = mypageLnb.querySelectorAll('.my-menu-item, .my-menu-sub__txt');
		menuItems.forEach($el => {
			let hasChild = $el.parentNode.children.length;
			// 하위메뉴가 있을경우 has-child 클래스 추가
			if (hasChild == '2') {
				$el.classList.add('has-child');
			}
			$el.addEventListener('click', function (e) {
				let hasChild = this.classList.contains('has-child');
				if (hasChild) {
					e.preventDefault();
				}
				let currentEl = this.parentElement;
				currentEl.classList.toggle('is-active');
				getSiblings(currentEl).forEach(function (subel, i) {
					subel.classList.remove('is-active');
					const subelClass = subel.querySelectorAll('.is-active');
					subelClass.forEach(subItem => {
						subItem.classList.remove('is-active');
					});
				});
			});
		});
	}

	// 행사 전시 헤더 작업 필요
	if (document.querySelectorAll('.gnb-link')) {
		let elGLink = document.querySelectorAll('.gnb-link');
		elGLink.forEach(target => {
			target.addEventListener('mouseenter', function (event) {
				event.stopPropagation();
				target.closest('.tmpl-header').classList.add('is-hover');
			});
		});
	}

	if (document.querySelector('.tmpl-header')) {
		let elVar = document.querySelector('.tmpl-header');
		elVar.addEventListener('mouseleave', function (event) {
			event.stopPropagation();
			elVar.classList.remove('is-hover');
		});
	}
});
//trade office
document.addEventListener('DOMContentLoaded', function () {
	// copy URL button
	if (document.querySelector('#copyUrl')) {
		const copyUrl = document.querySelector('#copyUrl');
		const linkUrl = document.querySelector('.trade-link__url');

		copyUrl.addEventListener('click', function () {
			console.log(linkUrl.innerText);
			navigator.clipboard.writeText(linkUrl.innerText);

			alert('Copied the URL: ' + linkUrl.innerText);
		});
	}

	// trade office Menu js
	// mobile menu open click event
	if (document.querySelector('.trade-mobile-menu')) {
		let mobileBtn = document.querySelector('.trade-mobile-menu');
		mobileBtn.addEventListener('click', () => {
			document.querySelector('html').classList.add('__mobile-menu');
		});
	}
	// mobile menu close click event
	if (document.querySelector('.trade-mobile-close')) {
		let asideClose = document.querySelector('.trade-mobile-close');
		asideClose.addEventListener('click', function (event) {
			event.stopPropagation();
			// 모바일 창 닫을 시 has-child 의 모든 is-active 제거
			const elMpChildLi = document.querySelectorAll('.trade-gnb .has-child');
			elMpChildLi.forEach(target => {
				target.classList.remove('is-active');
			});
			document.querySelector('html').classList.remove('__mobile-menu', '__allmenu-open');
			// 템플릿 모바일 메뉴
			if (document.querySelector('.trade-gnb')) {
				const el = document.querySelectorAll('.trade-gnb > ul > li');
				el.forEach(target => {
					target.classList.remove('is-open');
				});
			}
		});
	}

	const tradeAside = document.querySelector('.trade-aside');
	if (tradeAside) {
		let elGLink = tradeAside.querySelectorAll('.trade-gnb__link');
		elGLink.forEach(target => {
			addListenerMulti(target, 'mouseenter focusin', function (event) {
				event.stopPropagation();
				tradeAside.classList.add('is-hover');
			});
		});
		addListenerMulti(tradeAside, 'mouseleave focusout', function (event) {
			event.stopPropagation();
			if (event.type == 'focusout') {
				if (event.relatedTarget) {
					if (!event.relatedTarget.closest('aside')) {
						tradeAside.classList.remove('is-hover');
					}
				} else {
					tradeAside.classList.remove('is-hover');
				}
			} else {
				tradeAside.classList.remove('is-hover');
			}
		});
		// mobile menu
		const mobileMenu = tradeAside.querySelectorAll('.trade-gnb > ul > li');
		mobileMenu.forEach(target => {
			if (target.querySelector('li')) {
				target.classList.add('has-child');
			}
		});
		elGLink.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				getSiblings(target.closest('li')).forEach(function (subel, i) {
					subel.classList.remove('is-open');
				});
				target.parentNode.classList.toggle('is-open');
			});
		});
	}
});
// multi event 함수 ex. addListenerMulti(window, 'mousemove touchmove', function(){…});
function addListenerMulti(el, s, fn) {
	s.split(' ').forEach(e => el.addEventListener(e, fn, false));
}
