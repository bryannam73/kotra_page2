'use strict';

document.addEventListener('DOMContentLoaded', function () {
	// Modals
	// const rootEl = document.documentElement;
	const $modalButtons = getAll('.modal-button');
	// const $modalCloses = getAll('.modal .modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button');
	const $modalCloses = getAll('.modal .modal-background, .btn-modal-close');
	// const focusTarget = [];

	if ($modalButtons.length > 0) {
		$modalButtons.forEach(function ($el) {
			$el.addEventListener('click', function (event) {
				let parentModal = event.target.closest('.modal');
				let subModal;
				let btnModal = event.target;
				var target = $el.dataset.target;
				let modalFoot = event.target.closest('.modal-card-foot');

				if (parentModal && !modalFoot) {
					subModal = true;
				} else {
					subModal = false;
				}
				openModal(target, subModal, btnModal);
				// focusTarget = event.target;
			});
		});
	}

	if ($modalCloses.length > 0) {
		$modalCloses.forEach(function ($el) {
			$el.addEventListener('click', function (event) {
				let target = event.target.closest('.modal');
				closeModals(target);
			});
		});
	}

	// function openModal(target, subModal, btnModal) {
	// 	var $target = document.getElementById(target);
	// 	rootEl.classList.add('is-clipped');
	// 	$target.classList.add('is-active');

	// 	// 해당 modal 로 focus 이동
	// 	$target.querySelector('.modal-card').setAttribute('tabindex', 0);
	// 	$target.querySelector('.modal-card').focus();

	// 	// focusTarget = btnModal;
	// 	focusTarget.push(btnModal);
	// 	// $target.querySelector('.delete').focus();
	// 	if (subModal) {
	// 		$target.classList.add('sub-modal');
	// 	}
	// }

	// function closeModals(target) {
	// 	let activeModals = getAll('.modal.is-active');
	// 	if (target === 'Escape') {
	// 		// console.log(target);
	// 		rootEl.classList.remove('is-clipped');
	// 		activeModals.forEach(item => {
	// 			item.classList.remove('is-active');
	// 		});
	// 		// console.log(focusTarget[0]);
	// 		focusTarget[0].focus();
	// 		focusTarget.splice(0, focusTarget.length);
	// 	} else {
	// 		if (activeModals.length < 2) {
	// 			rootEl.classList.remove('is-clipped');
	// 		}
	// 		target.classList.remove('is-active', 'sub-modal');
	// 		target.querySelector('.modal-card').removeAttribute('tabindex');
	// 		focusTarget[focusTarget.length - 1].focus();
	// 		focusTarget.pop();
	// 	}
	// }

	// document.addEventListener('keydown', function (event) {
	// 	var e = event || window.event;

	// 	if (e.key === 'Escape') {
	// 		let modalActive = document.querySelector('.modal.is-active');
	// 		if (modalActive) {
	// 			closeModals('Escape');
	// 		}
	// 	}
	// });
});

const rootEl = document.documentElement;
const focusTarget = [];
function openModal(target, subModal, btnModal) {
	// console.log(target, subModal, btnModal, btnModal.target);
	var $target = document.getElementById(target);
	rootEl.classList.add('is-clipped');
	$target.classList.add('is-active');

	// 해당 modal 로 focus 이동
	$target.querySelector('.modal-card').setAttribute('tabindex', 0);
	$target.querySelector('.modal-card').focus();
	// modal 호출시 상단화면부터 scroll
	$target.querySelector('.modal-card-body').scrollTo(0, 0);
	
	focusTarget.push(btnModal);
	if (subModal) {
		$target.classList.add('sub-modal');
	}
}

function closeModals(target) {
	let activeModals = getAll('.modal.is-active');
	// if (target === 'Escape') {
	// 	rootEl.classList.remove('is-clipped');
	// 	activeModals.forEach(item => {
	// 		item.classList.remove('is-active');
	// 	});
	// 	focusTarget[0].focus();
	// 	focusTarget.splice(0, focusTarget.length);
	// } else {
	if (activeModals.length < 2) {
		rootEl.classList.remove('is-clipped');
	}
	target.classList.remove('is-active', 'sub-modal');
	target.querySelector('.modal-card').removeAttribute('tabindex');
	focusTarget[focusTarget.length - 1].focus();
	focusTarget.pop();
	// }
}

function modalCustomClose(target) {
	let activeModals = getAll('.modal.is-active');
	if (activeModals.length < 2) {
		rootEl.classList.remove('is-clipped');
	}
	// console.log(target.closest('.modal'));
	let currentModal = target.closest('.modal');
	currentModal.classList.remove('is-active', 'sub-modal');
	currentModal.querySelector('.modal-card').removeAttribute('tabindex');
	focusTarget[focusTarget.length - 1].focus();
	focusTarget.pop();
}
