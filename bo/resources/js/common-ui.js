'use strict';

//nav
function navSelector(token) {
	let current;
	return function(element) {
		// if(element !== current) {
		//     if(current) current.classList.remove(token);
		//     element.classList.add(token);
		//     current = element;
		// }
		if (current) current.classList.remove(token);
		element.classList.add(token);
		current = element;
	}
}
let navActive = navSelector('is-active');
// hover시 2뎁스 메뉴 활성화
const setNav = function setNav() {
	const gnb = document.getElementById('gnb');
	gnb.addEventListener('mouseover', (e) => {
		let target = e.target;
		let parent = target.parentElement;
		//console.log(target);
		if(parent.classList.contains('menu-item')) {
			navActive(parent);
		}
	});
}
function lnb() {
	Array.from(document.querySelectorAll('.menu-item.is-active')).forEach(function(e) { 
		e.classList.remove('is-active');
	});
};
// leave || scroll시 2뎁스 메뉴 비활성화
const getNav = function getNav() {
	gnb.addEventListener('mouseleave', lnb);
	document.addEventListener('scroll', lnb);
}

// 인풋, 텍스트 글자수 카운트 수정 : 천단위 콤마
let setTextCount = function (id, max) {
	let el = document.getElementById(id);
	el.addEventListener('keyup', function (e) {
		e.stopPropagation();
		let count = Number(el.value.length);
		//console.log(count.toLocaleString());
		if (count > max) {
			el.value = el.value.substr(0, max);
			//alert('글자 수가' + String(max) + '자를 초과했습니다.');
			alert('글자 수가' + max.toLocaleString() + '자를 초과했습니다.');
		} else {
			//let txtCount = String(count) + '/' + String(max);
			let txtCount = count.toLocaleString() + '/' + max.toLocaleString();
			el.nextElementSibling.querySelector('.count-txt').innerText = txtCount;
		}
	});
};

let setSearchToggle = function setSearchToggle() {
	var icon = document.getElementById('searchIcon');
	var search = document.getElementById('search');
	var input = document.getElementById('algoliaSearch');

	if (!icon) {
		return;
	}

	icon.addEventListener('click', function (event) {
		search.classList.toggle('bd-is-visible');

		if (search.classList.contains('bd-is-visible')) {
			algoliaSearch.focus();
		}
	});

	window.addEventListener('keydown', function (event) {
		if (event.key === 'Escape') {
			return search.classList.remove('bd-is-visible');
		}
	});
};

// Navbar
/*
var setNavbarVisibility = function setNavbarVisibility() {
	var navbar = document.getElementById('navbar');

	if (!navbar) {
		return;
	}

	var cs = getComputedStyle(navbar);
	var paddingX = parseFloat(cs.paddingLeft) + parseFloat(cs.paddingRight);
	var brand = navbar.querySelector('.navbar-brand');
	var end = navbar.querySelector('.navbar-end');
	var search = navbar.querySelector('.bd-search');
	var original = document.getElementById('navbarStartOriginal');
	var clone = document.getElementById('navbarStartClone');

	var rest = navbar.clientWidth - paddingX - brand.clientWidth - end.clientWidth - search.clientWidth;

	var space = original.clientWidth;
	var all = document.querySelectorAll('#navbarStartClone > .bd-navbar-item');
	var base = document.querySelectorAll('#navbarStartClone > .bd-navbar-item-base');
	var more = document.querySelectorAll('#navbarStartOriginal > .bd-navbar-item-more');
	var dropdown = document.querySelectorAll('#navbarStartOriginal .bd-navbar-dropdown > .navbar-item');

	var count = 0;
	var totalWidth = 0;

	var showMore = function showMore() {};

	var hideMore = function hideMore() {};

	var _iteratorNormalCompletion = true;
	var _didIteratorError = false;
	var _iteratorError = undefined;

	try {
		for (var _iterator = all[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
			var item = _step.value;

			totalWidth += item.offsetWidth;

			if (totalWidth > rest) {
				break;
			}

			count++;
		}
	} catch (err) {
		_didIteratorError = true;
		_iteratorError = err;
	} finally {
		try {
			if (!_iteratorNormalCompletion && _iterator.return) {
				_iterator.return();
			}
		} finally {
			if (_didIteratorError) {
				throw _iteratorError;
			}
		}
	}

	var howManyMore = Math.max(0, count - base.length);

	if (howManyMore > 0) {
		for (var i = 0; i < howManyMore; i++) {
			more[i].classList.add('bd-is-visible');
			dropdown[i].classList.add('bd-is-hidden');
		}
	}

	for (var j = howManyMore; j < more.length; j++) {
		more[j].classList.remove('bd-is-visible');
	}

	for (var k = howManyMore; k < dropdown.length; k++) {
		dropdown[k].classList.remove('bd-is-hidden');
	}
};
*/

document.addEventListener('DOMContentLoaded', function () {	
	setNav();
	getNav();
	
	// Dropdowns
	let $dropdowns = getAll('.dropdown:not(.is-hoverable)');
	const setDropdowns = function setDropdowns() {
		if ($dropdowns.length > 0) {
			$dropdowns.forEach(function ($el) {
				$el.addEventListener('click', function (event) {
					event.stopPropagation();
					let $dropdownHover = getAll('.dropdown:not(.is-hover)');
					$dropdownHover.forEach(function ($el) {
						$el.classList.remove('is-active');
					});
					$el.classList.toggle('is-active');
				});
				$el.addEventListener('mouseenter', function (event) {
					event.stopPropagation();
					$el.classList.toggle('is-hover');
				});
				$el.addEventListener('mouseleave', function (event) {
					event.stopPropagation();
					$el.classList.remove('is-hover');
				});
			});
			document.addEventListener('click', function (event) {
				closeDropdowns();
			});
		}
	};
	setSearchToggle();
	setDropdowns();

	function closeDropdowns() {
		$dropdowns.forEach(function ($el) {
			$el.classList.remove('is-active');
		});
	}	

	//DB 테이블 전체 토글 
	let $chk = false;
	let $checked = getAll('.chk-all');
	if ($checked.length > 0) {
		$checked.forEach(function ($el) {
			$el.addEventListener('click', function () {
				var chk = $el.checked;
				var parent = $el.closest('.is-check');
				var target = parent.querySelectorAll('.check-item');
				//console.log(chk);
				checkAll(target, chk);
			});
		});
	}
	function checkAll(target, chk) {	
		$chk = $chk ? false : true;
		for(var i = 0; i < target.length; i++) {
			target[i].checked = chk;
			//console.log($chk);
		}
	}
	
	// setNavbarVisibility();

	// Toggles
	// let $burgers = getAll('.navbar-burger');

	// if ($burgers.length > 0) {
	// 	$burgers.forEach(function ($el) {
	// 		if (!$el.dataset.target) {
	// 			return;
	// 		}

	// 		$el.addEventListener('click', function () {
	// 			var target = $el.dataset.target;
	// 			var $target = document.getElementById(target);
	// 			$el.classList.toggle('is-active');
	// 			$target.classList.toggle('is-active');
	// 		});
	// 	});
	// }

	// Cookies

	// var cookieBookModalName = 'bulma_closed_book_modal';
	//   var cookieBookModal = Cookies.getJSON(cookieBookModalName) || false;

	// Modals
	var rootEl = document.documentElement;
	var $modalButtons = getAll('.modal-button');
	//var $modalCloses = getAll('.modal .modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button');
	var $modalCloses = getAll('.modal .modal-background, .modal-close, .modal-card-head .delete');
	var focusTarget = [];

	if ($modalButtons.length > 0) {
		$modalButtons.forEach(function ($el) {
			$el.addEventListener('click', function (event) {
				let parentModal = event.target.closest('.modal');
				let subModal;
				let btnModal = event.target;
				var target = $el.dataset.target;
				let modalFoot = event.target.closest('.modal-card-foot');

				if (parentModal && !modalFoot) {
					subModal = true;
				} else {
					subModal = false;
				}
				openModal(target, subModal, btnModal);

				// focusTarget = event.target;
			});
		});
	}

	if ($modalCloses.length > 0) {
		$modalCloses.forEach(function ($el) {
			$el.addEventListener('click', function (event) {
				let target = event.target.closest('.modal');
				closeModals(target);
			});
		});
	}

	function openModal(target, subModal, btnModal) {
		var $target = document.getElementById(target);
		rootEl.classList.add('is-clipped');
		$target.classList.add('is-active');

		// 해당 modal 로 focus 이동
		$target.querySelector('.modal-card').setAttribute('tabindex', 0);
		$target.querySelector('.modal-card').focus();

		// focusTarget = btnModal;
		focusTarget.push(btnModal);
		// $target.querySelector('.delete').focus();
		if (subModal) {
			$target.classList.add('sub-modal');
		}
	}

	function closeDropdowns() {
		$dropdowns.forEach(function ($el) {
			$el.classList.remove('is-active');
		});
	}

	function closeModals(target) {
		let activeModals = getAll('.modal.is-active');
		if (target === 'Escape') {
			console.log(target);
			rootEl.classList.remove('is-clipped');
			activeModals.forEach(item => {
				item.classList.remove('is-active');
			});
			//2022-12-09 수정 focus 제외
			//focusTarget[0].focus();
			focusTarget.splice(0, focusTarget.length);
		} else {
			if (activeModals.length < 2) {
				rootEl.classList.remove('is-clipped');
			}
			target.classList.remove('is-active', 'sub-modal');
			target.querySelector('.modal-card').removeAttribute('tabindex');
			//focusTarget[focusTarget.length - 1].focus();
			//focusTarget.pop();
		}
	}

	document.addEventListener('keydown', function (event) {
		var e = event || window.event;

		if (e.key === 'Escape') {
			let modalActive = document.querySelector('.modal.is-active');
			let dropdownActive = document.querySelector('.dropdown.is-active');
			if (modalActive) {
				closeModals('Escape');
			}
			if (dropdownActive) {
				closeDropdowns();
			}
		}
	});

	// Events
	var resizeTimer = void 0;

	function handleResize() {
		window.clearTimeout(resizeTimer);

		// resizeTimer = window.setTimeout(function () {
		// 	setNavbarVisibility();
		// }, 10);
	}

	window.addEventListener('resize', handleResize);

	// Spinner box
	var setUiSpinner = function uiSpinner() {
		const $spinner = document.querySelectorAll('[data-js-spinner]');

		Array.prototype.forEach.call($spinner, function ($element) {
			const $input = $element.querySelector('[data-js-spinner-input]');
			const $btnDecrement = $element.querySelector('[data-js-spinner-decrement]');
			const $btnIncrement = $element.querySelector('[data-js-spinner-increment]');
			const inputRules = {
				min: +$input.getAttribute('min'),
				max: +$input.getAttribute('max'),
				steps: +$input.getAttribute('steps') || 1,
				maxlength: +$input.getAttribute('maxlength') || null,
				minlength: +$input.getAttribute('minlength') || null,
			};
			let inputValue = +$input.value || 0;

			$input.addEventListener('input', handleInputUpdateValueInput, false);
			$element.addEventListener('keydown', handleMousedownDecrementSpinner, false);
			$btnDecrement.addEventListener('click', handleClickDecrementBtnDecrement, false);
			$btnIncrement.addEventListener('click', handleClickIncrementBtnIncrement, false);

			function handleInputUpdateValueInput() {
				let value = +$input.value;

				if (isNaN(value)) inputValue = 0;
				else inputValue = value;
			}

			function handleMousedownDecrementSpinner(event) {
				const keyCode = event.keyCode;
				const arrowUpKeyCode = 38;
				const arrowDownKeyCode = 40;

				if (keyCode === arrowDownKeyCode) handleClickDecrementBtnDecrement();
				else if (keyCode === arrowUpKeyCode) handleClickIncrementBtnIncrement();
			}

			function handleClickDecrementBtnDecrement() {
				if (!isGreaterThanMaxlength(inputValue - 1)) {
					if ($input.hasAttribute('min')) {
						if (inputValue > inputRules.min) decrement();
					} else decrement();
				}
			}

			function handleClickIncrementBtnIncrement() {
				if (!isGreaterThanMaxlength(inputValue + 1)) {
					if ($input.hasAttribute('max')) {
						if (inputValue < inputRules.max) increment();
					} else increment();
				}
			}

			function decrement() {
				inputValue -= inputRules.steps;
				if ($input.hasAttribute('max') && inputValue > $input.getAttribute('max')) {
					inputValue = +$input.getAttribute('max');
					$input.value = +inputValue.toFixed(12);
				} else $input.value = +inputValue.toFixed(12);
			}

			function increment() {
				inputValue += inputRules.steps;
				if ($input.hasAttribute('min') && inputValue < $input.getAttribute('min')) {
					inputValue = +$input.getAttribute('min');
					$input.value = +inputValue.toFixed(12);
				} else $input.value = +inputValue.toFixed(12);
			}

			function isGreaterThanMaxlength(value) {
				return value.toString().length > inputRules.maxlength && inputRules.maxlength !== null;
			}
		});
	};

	setUiSpinner();

	// Collapse / Accordion
	function initCollapse(elem, option) {
		document.addEventListener('click', function (e) {
			if (!e.target.matches(elem + ' [data-collapse]')) return;
			else {
				e.preventDefault();
				let parentEl = e.target.parentElement.closest('.collapse-item');
				if (parentEl.classList.contains('is-active')) {
					parentEl.classList.remove('is-active');
				} else {
					// accordion
					if (option == true) {
						let accId = parentEl.getAttribute('data-accordion');
						let accEl = document.getElementById(accId);
						let elementList = accEl.querySelectorAll('.collapse-item');
						Array.prototype.forEach.call(elementList, function (e) {
							e.classList.remove('is-active');
						});
					} 
					parentEl.classList.add('is-active');
				}
			}
		});
	}
	initCollapse('.collapse.is-accordion', true);
	initCollapse('.collapse.is-collapse', false);
	//initCollapse('.collapse.is-collapse.is-seacrh', 'search');
	
});

window.addEventListener('load', function () {
	// all menu 선택용 임시 스크립트
	// if (document.querySelector('.allmenu-button')) {
	// 	let ButtonAllMenu = document.querySelector('.allmenu-button');
	// 	ButtonAllMenu.addEventListener('click', function (event) {
	// 		event.stopPropagation();
	// 		ButtonAllMenu.classList.toggle('is-active');
	// 		document.body.classList.toggle('__allmenu-open');
	// 	});
	// }
});

function closeDropdowns() {
	$dropdowns.forEach(function ($el) {
		$el.classList.remove('is-active');
	});
}

// 웹접근성 Dynamic Tab
function TabsAutomatic(groupNode) {
	this.tablistNode = groupNode;

	this.tabs = [];

	this.firstTab = null;
	this.lastTab = null;

	// ie11
	let tabs = [];
	new Set(this.tablistNode.querySelectorAll('[role=tab]')).forEach(function (v) {
		tabs.push(v);
	});
	this.tabs = tabs;
	// this.tabs = Array.from(this.tablistNode.querySelectorAll('[role=tab]'));

	this.tabpanels = [];

	for (var i = 0; i < this.tabs.length; i += 1) {
		var tab = this.tabs[i];
		var tabpanel = document.getElementById(tab.getAttribute('aria-controls'));

		tab.tabIndex = -1;
		tab.setAttribute('aria-selected', 'false');
		this.tabpanels.push(tabpanel);

		tab.addEventListener('keydown', this.onKeydown.bind(this));
		tab.addEventListener('click', this.onClick.bind(this));

		if (!this.firstTab) {
			this.firstTab = tab;
		}
		this.lastTab = tab;
	}
	this.setSelectedTab(this.firstTab, false);
}
TabsAutomatic.prototype.setSelectedTab = function (currentTab, setFocus) {
	if (typeof setFocus !== 'boolean') {
		setFocus = true;
	}
	for (var i = 0; i < this.tabs.length; i += 1) {
		var tab = this.tabs[i];
		if (currentTab === tab) {
			tab.setAttribute('aria-selected', 'true');
			tab.parentElement.classList.add('is-active');
			tab.removeAttribute('tabindex');
			this.tabpanels[i].setAttribute('aria-hidden', 'false');
			this.tabpanels[i].classList.add('is-active');
			if (setFocus) {
				tab.focus();
			}
		} else {
			tab.parentElement.classList.remove('is-active');
			tab.setAttribute('aria-selected', 'false');
			tab.tabIndex = -1;
			this.tabpanels[i].setAttribute('aria-hidden', 'true');
			this.tabpanels[i].classList.remove('is-active');
		}
	}
};
TabsAutomatic.prototype.setSelectedToPreviousTab = function (currentTab) {
	var index;

	if (currentTab === this.firstTab) {
		this.setSelectedTab(this.lastTab);
	} else {
		index = this.tabs.indexOf(currentTab);
		this.setSelectedTab(this.tabs[index - 1]);
	}
};
TabsAutomatic.prototype.setSelectedToNextTab = function (currentTab) {
	var index;

	if (currentTab === this.lastTab) {
		this.setSelectedTab(this.firstTab);
	} else {
		index = this.tabs.indexOf(currentTab);
		this.setSelectedTab(this.tabs[index + 1]);
	}
};
/* EVENT HANDLERS */
TabsAutomatic.prototype.onKeydown = function (event) {
	var tgt = event.currentTarget,
		flag = false;

	switch (event.key) {
		case 'ArrowLeft':
			this.setSelectedToPreviousTab(tgt);
			flag = true;
			break;

		case 'ArrowRight':
			this.setSelectedToNextTab(tgt);
			flag = true;
			break;

		case 'Home':
			this.setSelectedTab(this.firstTab);
			flag = true;
			break;

		case 'End':
			this.setSelectedTab(this.lastTab);
			flag = true;
			break;

		default:
			break;
	}

	if (flag) {
		event.stopPropagation();
		event.preventDefault();
	}
};
TabsAutomatic.prototype.onClick = function (event) {
	this.setSelectedTab(event.currentTarget);
};
// Initialize tablist
window.addEventListener('load', function () {
	var tablists = document.querySelectorAll('[role=tablist]');
	for (var i = 0; i < tablists.length; i++) {
		// TabsAutomatic(tablists[i]);
		new TabsAutomatic(tablists[i]);
	}
});

// Utils
function getAll(selector) {
	var parent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

	return Array.prototype.slice.call(parent.querySelectorAll(selector), 0);
}

// var world = document.getElementById('world'); var closest = closest(world, 'item');
// function closest(el, classname) {
// 	if (el.parentNode) {
// 		if (el.parentNode.className.includes(classname)) {
// 			return el.parentNode;
// 		} else {
// 			return closest(el.parentNode, classname);
// 		}
// 	} else {
// 		return false;
// 	}
// }

/*
s : mp menu : Products Categories
*/
// get all siblings
function getSiblings(e) {
	// for collecting siblings
	let siblings = [];
	// if no parent, return no sibling
	if (!e.parentNode) {
		return siblings;
	}
	// first child of the parent node
	let sibling = e.parentNode.firstChild;

	// collecting siblings
	while (sibling) {
		if (sibling.nodeType === 1 && sibling !== e) {
			siblings.push(sibling);
		}
		sibling = sibling.nextSibling;
	}
	return siblings;
}

function extend(a, b) {
	for (var key in b) {
		if (b.hasOwnProperty(key)) {
			a[key] = b[key];
		}
	}
	return a;
}
function hasParent(e, id) {
	if (!e) return false;
	var el = e.target || e.srcElement || e || false;
	while (el && el.id != id) {
		el = el.parentNode || false;
	}
	return el !== false;
}
// returns the depth of the element "e" relative to element with id=id
// for this calculation only parents with classname = waypoint are considered
function getLevelDepth(e, id, waypoint, cnt) {
	cnt = cnt || 0;
	if (e.id.indexOf(id) >= 0) return cnt;

	if (e.classList.contains(waypoint)) {
		++cnt;
	}
	return e.parentNode && getLevelDepth(e.parentNode, id, waypoint, cnt);
}

// http://coveroverflow.com/a/11381730/989439
function mobilecheck() {
	var check = false;
	(function (a) {
		if (
			/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) ||
			/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
				a.substr(0, 4)
			)
		)
			check = true;
	})(navigator.userAgent || navigator.vendor || window.opera);
	return check;
}

// returns the closest element to 'e' that has class "classname"
function closest(e, classname) {
	if (e.classList.contains(classname)) {
		return e;
	}
	return e.parentNode && closest(e.parentNode, classname);
}

function mlPushMenu(el, trigger, options) {
	this.el = el;
	this.trigger = trigger;
	this.options = extend(this.defaults, options);
	// support 3d transforms
	this._init();
}

mlPushMenu.prototype = {
	defaults: {
		type: 'overlap', // overlap || cover
		// space between each overlaped level
		levelSpacing: 40,
		backClass: 'mp-back',
	},
	_init: function () {
		this.open = false;
		this.level = 0;
		// this.wrapper = document.getElementById('wrap');
		this.wrapper = document.body;
		this.levels = Array.prototype.slice.call(this.el.querySelectorAll('.mp-level'));

		// li 요소별 하위에 mp-level 클래스가 있는 경우 has-child 부여
		this.lis = Array.prototype.slice.call(this.el.querySelectorAll('li'));
		this.lis.forEach(function (el, i) {
			try {
				if (el.querySelector('section').classList.contains('mp-level')) {
					el.classList.add('has-child');
				}
			} catch (error) {
				// console.log(error);
			}
		});

		var self = this;
		this.elWidth = 300;
		this.levels.forEach(function (el, i) {
			el.setAttribute('data-level', getLevelDepth(el, self.el.id, 'mp-level'));
		});
		this.menuItems = Array.prototype.slice.call(this.el.querySelectorAll('li'));
		this.levelBack = Array.prototype.slice.call(this.el.querySelectorAll('.' + this.options.backClass));
		this.eventtype = mobilecheck() ? 'touchstart' : 'click';
		this.el.classList.add('mp-' + this.options.type);
		this._initEvents();
	},
	_initEvents: function () {
		var self = this;

		var bodyClickFn = function (el) {
			self._resetMenu();
			el.removeEventListener(self.eventtype, bodyClickFn);
		};

		this.trigger.addEventListener(this.eventtype, function (ev) {
			ev.stopPropagation();
			ev.preventDefault();
			if (self.open) {
				self._resetMenu();
			} else {
				self._openMenu();
				document.addEventListener(self.eventtype, function (ev) {
					if (self.open && !hasParent(ev.target, self.el.id)) {
						bodyClickFn(this);
					}
				});
			}
		});

		let windowWidth = window.outerWidth;
		console.log(windowWidth);

		this.menuItems.forEach(function (el, i) {
			var subLevel = el.querySelector('.mp-level');
			if (subLevel) {
				// 1depth 메뉴 a 태그 클릭시
				el.querySelector('a').addEventListener(self.eventtype, function (ev) {
					ev.preventDefault();
					var level = closest(el, 'mp-level').getAttribute('data-level');
					if (self.level <= level) {
						ev.stopPropagation();
						closest(el, 'mp-level').classList.add('mp-level-overlay');
						self._openMenu(subLevel);
					}
					// li 요소의 형제 요소들 모두 mp-level-open 제거 후 본인에만 추가
					// el.classList.add('mp-level-open');
					// getSiblings(el).forEach(function (subel, i) {
					// 	subel.classList.remove('mp-level-open');
					// });
				});
				// 1depth 메뉴 a 태그 mouseover
				el.querySelector('a').addEventListener('mouseenter', function (ev) {
					ev.preventDefault();
					// li 요소의 형제 요소들 모두 mp-level-open 제거 후 본인에만 추가
					el.classList.add('is-hover');
					getSiblings(el).forEach(function (subel, i) {
						subel.classList.remove('is-hover');
					});
				});
			}
		});

		this.levels.forEach(function (el, i) {
			el.addEventListener(self.eventtype, function (ev) {
				ev.stopPropagation();
				var level = el.getAttribute('data-level');
				if (self.level > level) {
					self.level = level;
					self._closeMenu();
				}
			});
		});

		this.levelBack.forEach(function (el, i) {
			el.addEventListener(self.eventtype, function (ev) {
				ev.preventDefault();
				var level = closest(el, 'mp-level').getAttribute('data-level');
				if (self.level <= level) {
					ev.stopPropagation();
					self.level = closest(el, 'mp-level').getAttribute('data-level') - 1;
					self.level === 0 ? self._resetMenu() : self._closeMenu();
				}
			});
		});
	},
	_openMenu: function (subLevel) {
		++this.level;

		var levelFactor = (this.level - 1) * this.options.levelSpacing,
			translateVal = this.options.type === 'overlap' ? this.elWidth + levelFactor : this.elWidth;
		this._setTransform('translate3d(' + translateVal + 'px,0,0)');

		if (subLevel) {
			this._setTransform('', subLevel);
			for (var i = 0, len = this.levels.length; i < len; ++i) {
				var levelEl = this.levels[i];
				// console.log(' levelEl ', levelEl);
				if (levelEl != subLevel && !levelEl.classList.contains('mp-level-open')) {
					this._setTransform('translate3d(-100%,0,0) translate3d(' + -1 * levelFactor + 'px,0,0)', levelEl);
				}
			}
		}
		if (this.level === 1) {
			this.wrapper.classList.add('__allmenu-open');
			this.open = true;
		}
		(subLevel || this.levels[0]).classList.add('mp-level-open');
	},
	_resetMenu: function () {
		this._setTransform('translate3d(0,0,0)');
		this.level = 0;
		// remove class mp-pushed from main wrapper
		this.wrapper.classList.remove('__allmenu-open');
		this._toggleLevels();
		this.open = false;
	},
	_closeMenu: function () {
		var translateVal = this.options.type === 'overlap' ? this.elWidth + (this.level - 1) * this.options.levelSpacing : this.elWidth;
		this._setTransform('translate3d(' + translateVal + 'px,0,0)');
		this._toggleLevels();
	},
	_setTransform: function (val, el) {
		// el = el || this.wrapper;
		el = el || this.el;
		el.style.WebkitTransform = val;
		el.style.MozTransform = val;
		el.style.transform = val;
	},
	_toggleLevels: function () {
		for (var i = 0, len = this.levels.length; i < len; ++i) {
			var levelEl = this.levels[i];
			if (levelEl.getAttribute('data-level') >= this.level + 1) {
				levelEl.classList.remove('mp-level-open');
				levelEl.classList.remove('mp-level-overlay');
			} else if (Number(levelEl.getAttribute('data-level')) == this.level) {
				levelEl.classList.remove('mp-level-overlay');
			}
		}
	},
};

// add to global namespace
if (document.getElementById('mp-menu')) {
	window.mlPushMenu = mlPushMenu;
	new mlPushMenu(document.getElementById('mp-menu'), document.getElementById('trigger'), {
		type: 'cover',
	});
}
/*
 e : mp menu
*/